var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from "react";
import { getCurrentUser } from "../auth/authService";
import { Nav, NavDropdown, Navbar, Dropdown } from "react-bootstrap";
import { connect } from "react-redux";
import Avatar from "react-avatar";
import lightArrow from '../assets/images/icons/light-arrow.svg';
//import { PREVIOUS_USER } from "../accountAndSettings/loginAs/loginAsModal";
// import LoginAs from "../accountAndSettings/loginAs/loginAs"
import { FixedSizeList as List } from 'react-window';

var mapStateToProps = function mapStateToProps(state) {
  return {
    profile: state.profile
  };
};

var UserProfile = function (_Component) {
  _inherits(UserProfile, _Component);

  function UserProfile() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, UserProfile);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = UserProfile.__proto__ || Object.getPrototypeOf(UserProfile)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      firstName: "",
      lastName: "",
      email: "",
      orgFilter: "",
      moreFlag: false
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(UserProfile, [{
    key: "getPreviousUser",
    value: function getPreviousUser() {
      // return localStorage.getItem(PREVIOUS_USER);
      return "abcd";
    }
  }, {
    key: "returnName",
    value: function returnName() {
      var firstName = this.props.profile.first_name || "";
      var name = firstName;
      var email = this.props.profile.email;
      if (name === " ") {
        return email;
      } else {
        return name;
      }
    }
  }, {
    key: "openAccountAndSettings",
    value: function openAccountAndSettings() {
      this.props.history.push({
        pathname: "/orgs/" + this.props.organizationId + "/manage",
        search: this.props.location.search
      });
    }
  }, {
    key: "exploreProducts",
    value: function exploreProducts() {
      var _this2 = this;

      return React.createElement(
        "div",
        { className: "settings-item", onClick: function onClick() {
            return _this2.props.history.push({
              pathname: "/orgs/" + _this2.props.organizationId + "/products"
            });
          } },
        React.createElement(
          "span",
          { className: "settings-label" },
          React.createElement("img", { src: process.env.REACT_APP_SOCKET_ICON, alt: "sokt-icon", height: "18px", width: "18px", className: "mr-10" }),
          "Explore Products"
        )
      );
    }
  }, {
    key: "renderBilling",
    value: function renderBilling() {
      var _this3 = this;

      return React.createElement(
        "div",
        { className: "settings-item", onClick: function onClick() {
            return _this3.props.history.push({
              pathname: "/orgs/" + _this3.props.organizationId + "/billing"
            });
          } },
        React.createElement(
          "span",
          { className: "settings-label" },
          React.createElement(
            "svg",
            { width: "18", height: "18", viewBox: "0 0 18 18", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
            React.createElement("path", { d: "M9.75 1.5H4.5C4.10217 1.5 3.72064 1.65804 3.43934 1.93934C3.15804 2.22064 3 2.60218 3 3V15C3 15.3978 3.15804 15.7794 3.43934 16.0607C3.72064 16.342 4.10217 16.5 4.5 16.5H13.5C13.8978 16.5 14.2794 16.342 14.5607 16.0607C14.842 15.7794 15 15.3978 15 15V6.75L9.75 1.5Z", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
            React.createElement("path", { d: "M9.75 1.5V6.75H15", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
            React.createElement("path", { d: "M8.2983 14.7273H8.96875L8.97443 14.071C10.2045 13.9773 10.9176 13.3239 10.9205 12.3381C10.9176 11.3693 10.1875 10.8551 9.17614 10.6278L9.00852 10.5881L9.01989 9.16761C9.39773 9.25568 9.62784 9.49716 9.66193 9.85511H10.8409C10.8267 8.91477 10.125 8.24148 9.03125 8.12216L9.03693 7.45455H8.36648L8.3608 8.11648C7.25 8.22443 6.46591 8.89489 6.47159 9.86364C6.46875 10.7216 7.07386 11.2131 8.05682 11.4489L8.32955 11.517L8.31534 13.0199C7.85227 12.9318 7.53977 12.6477 7.50852 12.1733H6.31818C6.34659 13.321 7.09943 13.9631 8.30398 14.0682L8.2983 14.7273ZM8.9858 13.0199L8.99716 11.6932C9.4375 11.8324 9.67614 12.0114 9.67898 12.3352C9.67614 12.679 9.41477 12.9347 8.9858 13.0199ZM8.33807 10.4148C7.98295 10.2926 7.72727 10.108 7.73295 9.78125C7.73295 9.47727 7.94886 9.24148 8.34943 9.15909L8.33807 10.4148Z", fill: "black" })
          ),
          "Billing"
        )
      );
    }
  }, {
    key: "renderSettings",
    value: function renderSettings() {
      var _this4 = this;

      return React.createElement(
        "div",
        { className: "settings-item", onClick: function onClick() {
            return _this4.props.history.push({
              pathname: "/orgs/" + _this4.props.organizationId + "/manage/authkeys"
            });
          } },
        React.createElement(
          "span",
          { className: "settings-label" },
          React.createElement(
            "svg",
            { width: "18", height: "18", viewBox: "0 0 18 18", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
            React.createElement("path", { d: "M9 11.25C10.2426 11.25 11.25 10.2426 11.25 9C11.25 7.75736 10.2426 6.75 9 6.75C7.75736 6.75 6.75 7.75736 6.75 9C6.75 10.2426 7.75736 11.25 9 11.25Z", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
            React.createElement("path", { d: "M14.55 11.25C14.4502 11.4762 14.4204 11.7271 14.4645 11.9704C14.5086 12.2137 14.6246 12.4382 14.7975 12.615L14.8425 12.66C14.982 12.7993 15.0926 12.9647 15.1681 13.1468C15.2436 13.3289 15.2824 13.5241 15.2824 13.7212C15.2824 13.9184 15.2436 14.1136 15.1681 14.2957C15.0926 14.4778 14.982 14.6432 14.8425 14.7825C14.7032 14.922 14.5378 15.0326 14.3557 15.1081C14.1736 15.1836 13.9784 15.2224 13.7812 15.2224C13.5841 15.2224 13.3889 15.1836 13.2068 15.1081C13.0247 15.0326 12.8593 14.922 12.72 14.7825L12.675 14.7375C12.4982 14.5646 12.2737 14.4486 12.0304 14.4045C11.7871 14.3604 11.5362 14.3902 11.31 14.49C11.0882 14.5851 10.899 14.7429 10.7657 14.9441C10.6325 15.1454 10.561 15.3812 10.56 15.6225V15.75C10.56 16.1478 10.402 16.5294 10.1207 16.8107C9.83935 17.092 9.45782 17.25 9.06 17.25C8.66217 17.25 8.28064 17.092 7.99934 16.8107C7.71803 16.5294 7.56 16.1478 7.56 15.75V15.6825C7.55419 15.4343 7.47384 15.1935 7.32938 14.9915C7.18493 14.7896 6.98305 14.6357 6.75 14.55C6.52379 14.4502 6.27285 14.4204 6.02956 14.4645C5.78626 14.5086 5.56176 14.6246 5.385 14.7975L5.34 14.8425C5.20069 14.982 5.03526 15.0926 4.85316 15.1681C4.67106 15.2436 4.47587 15.2824 4.27875 15.2824C4.08163 15.2824 3.88644 15.2436 3.70434 15.1681C3.52224 15.0926 3.35681 14.982 3.2175 14.8425C3.07803 14.7032 2.9674 14.5378 2.89191 14.3557C2.81642 14.1736 2.77757 13.9784 2.77757 13.7812C2.77757 13.5841 2.81642 13.3889 2.89191 13.2068C2.9674 13.0247 3.07803 12.8593 3.2175 12.72L3.2625 12.675C3.4354 12.4982 3.55139 12.2737 3.5955 12.0304C3.63962 11.7871 3.60984 11.5362 3.51 11.31C3.41493 11.0882 3.25707 10.899 3.05585 10.7657C2.85463 10.6325 2.61884 10.561 2.3775 10.56H2.25C1.85217 10.56 1.47064 10.402 1.18934 10.1207C0.908035 9.83935 0.75 9.45782 0.75 9.06C0.75 8.66217 0.908035 8.28064 1.18934 7.99934C1.47064 7.71803 1.85217 7.56 2.25 7.56H2.3175C2.56575 7.55419 2.8065 7.47384 3.00847 7.32938C3.21044 7.18493 3.36429 6.98305 3.45 6.75C3.54984 6.52379 3.57962 6.27285 3.5355 6.02956C3.49139 5.78626 3.3754 5.56176 3.2025 5.385L3.1575 5.34C3.01803 5.20069 2.9074 5.03526 2.83191 4.85316C2.75642 4.67106 2.71757 4.47587 2.71757 4.27875C2.71757 4.08163 2.75642 3.88644 2.83191 3.70434C2.9074 3.52224 3.01803 3.35681 3.1575 3.2175C3.29681 3.07803 3.46224 2.9674 3.64434 2.89191C3.82644 2.81642 4.02163 2.77757 4.21875 2.77757C4.41587 2.77757 4.61106 2.81642 4.79316 2.89191C4.97526 2.9674 5.14069 3.07803 5.28 3.2175L5.325 3.2625C5.50176 3.4354 5.72626 3.55139 5.96956 3.5955C6.21285 3.63962 6.46379 3.60984 6.69 3.51H6.75C6.97183 3.41493 7.16101 3.25707 7.29427 3.05585C7.42752 2.85463 7.49904 2.61884 7.5 2.3775V2.25C7.5 1.85217 7.65803 1.47064 7.93934 1.18934C8.22064 0.908035 8.60217 0.75 9 0.75C9.39782 0.75 9.77935 0.908035 10.0607 1.18934C10.342 1.47064 10.5 1.85217 10.5 2.25V2.3175C10.501 2.55884 10.5725 2.79463 10.7057 2.99585C10.839 3.19707 11.0282 3.35493 11.25 3.45C11.4762 3.54984 11.7271 3.57962 11.9704 3.5355C12.2137 3.49139 12.4382 3.3754 12.615 3.2025L12.66 3.1575C12.7993 3.01803 12.9647 2.9074 13.1468 2.83191C13.3289 2.75642 13.5241 2.71757 13.7212 2.71757C13.9184 2.71757 14.1136 2.75642 14.2957 2.83191C14.4778 2.9074 14.6432 3.01803 14.7825 3.1575C14.922 3.29681 15.0326 3.46224 15.1081 3.64434C15.1836 3.82644 15.2224 4.02163 15.2224 4.21875C15.2224 4.41587 15.1836 4.61106 15.1081 4.79316C15.0326 4.97526 14.922 5.14069 14.7825 5.28L14.7375 5.325C14.5646 5.50176 14.4486 5.72626 14.4045 5.96956C14.3604 6.21285 14.3902 6.46379 14.49 6.69V6.75C14.5851 6.97183 14.7429 7.16101 14.9441 7.29427C15.1454 7.42752 15.3812 7.49904 15.6225 7.5H15.75C16.1478 7.5 16.5294 7.65803 16.8107 7.93934C17.092 8.22064 17.25 8.60217 17.25 9C17.25 9.39782 17.092 9.77935 16.8107 10.0607C16.5294 10.342 16.1478 10.5 15.75 10.5H15.6825C15.4412 10.501 15.2054 10.5725 15.0041 10.7057C14.8029 10.839 14.6451 11.0282 14.55 11.25V11.25Z", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" })
          ),
          "Auth Keys"
        )
      );
    }
  }, {
    key: "getCurrentOrg",
    value: function getCurrentOrg() {
      return this.props.organizations[this.props.organizationId];
    }
  }, {
    key: "renderAvatarWithOrg",
    value: function renderAvatarWithOrg(_onClick, ref1) {
      var currentOrg = this.getCurrentOrg();
      return React.createElement(
        "div",
        { className: "profile-box", onClick: function onClick(e) {
            e.preventDefault();
            _onClick(e);
          } },
        React.createElement(Avatar, { color: '#343a40', name: this.returnName(), size: 24, round: "30px" }),
        React.createElement(
          "span",
          { className: "px-3 org-name" },
          currentOrg.name
        ),
        React.createElement("img", {
          ref: ref1,
          src: lightArrow,
          alt: "settings-gear",
          className: "transition"
        })
      );
    }
  }, {
    key: "getUserDetails",
    value: function getUserDetails() {
      var firstName = this.props.profile.first_name || "";
      var lastName = this.props.profile.last_name || "";
      var name = firstName + " " + lastName;
      var email = this.props.profile.email;
      return { email: email, name: name };
    }
  }, {
    key: "getAllOrgs",
    value: function getAllOrgs() {
      var orgsArray = Object.values(this.props.organizations || {});
      var orgFilter = this.state.orgFilter;

      var filteredOrgsArray = orgsArray.filter(function (org) {
        return org.name.toLowerCase().includes(orgFilter.toLowerCase());
      }).sort(function (a, b) {
        if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;else if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;else return 0;
      });

      return filteredOrgsArray;
    }
  }, {
    key: "setShowFlag",
    value: function setShowFlag() {
      var orgFilter = this.state.orgFilter;
      var moreFlag = !this.state.moreFlag;
      if (!moreFlag) {
        orgFilter = "";
      }
      this.setState({ orgFilter: orgFilter, moreFlag: moreFlag });
    }
  }, {
    key: "setOrgFilter",
    value: function setOrgFilter(orgFilter) {
      this.setState({ orgFilter: orgFilter });
    }
  }, {
    key: "getItemCount",
    value: function getItemCount(orgCount) {
      var showFlag = this.state.moreFlag;
      if (orgCount > 5 && !showFlag) {
        return 5;
      } else {
        return orgCount;
      }
    }
  }, {
    key: "switchOrgRoute",
    value: function switchOrgRoute(id) {
      var pathname = "";
      var pathArray = this.props.history.location.pathname.split('/');
      if (pathArray[3] === 'manage') {
        pathname = "/orgs/" + id + "/manage";
      } else {
        pathname = "/orgs/" + id;
      }
      this.props.history.push({
        pathname: pathname
      });
    }
  }, {
    key: "renderOrgList",
    value: function renderOrgList() {
      var _this5 = this;

      var orgsLength = Object.keys(this.props.organizations || {}).length;
      var filteredOrgsArray = this.getAllOrgs();
      filteredOrgsArray = filteredOrgsArray.filter(function (org) {
        return org.id !== _this5.props.organizationId;
      });

      var orgItem = function orgItem(_ref2) {
        var index = _ref2.index,
            style = _ref2.style;

        var item = filteredOrgsArray[index];
        return React.createElement(
          "div",
          {
            key: item.id,
            className: "d-flex align-items-center p-2 settings-item justify-space-between",
            style: style,
            onClick: function onClick() {
              _this5.switchOrgRoute(item.id);
              _this5.props.switch_org(item.id);
            }
          },
          React.createElement(
            Dropdown.Item,
            null,
            React.createElement(
              "div",
              { "class": "d-flex justify-space-between" },
              React.createElement(
                "div",
                { className: "body-3 text-black text-truncate pl-2" },
                item.name
              ),
              React.createElement(
                "div",
                { className: "orgs-icon" },
                React.createElement(
                  "svg",
                  { width: "18", height: "18", viewBox: "0 0 18 18", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
                  React.createElement("path", { d: "M6.75 13.5L11.25 9L6.75 4.5", stroke: "#4F4F4F", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" })
                )
              )
            )
          )
        );
      };

      return orgsLength > 1 && React.createElement(
        "div",
        null,
        React.createElement(
          "div",
          { className: "text-uppercase text-sm-bold" },
          "SWITCH ORGS"
        ),
        React.createElement(
          "div",
          { className: "orgs-list profile-sm-block" },
          this.state.moreFlag && React.createElement(
            "div",
            null,
            React.createElement("input", {
              className: "form-control",
              onChange: function onChange(e) {
                return _this5.setOrgFilter(e.target.value, filteredOrgsArray.length || 0);
              },
              value: this.state.orgFilter,
              placeholder: "Search"
            })
          ),
          filteredOrgsArray.length == 0 ? React.createElement(
            "div",
            { className: "pb-2 text-center w-100" },
            React.createElement(
              "small",
              { className: "body-6" },
              "No Organizations Found"
            )
          ) : React.createElement(
            List,
            { height: filteredOrgsArray.length < 5 ? 36 * filteredOrgsArray.length : 220, itemCount: this.getItemCount(filteredOrgsArray.length), itemSize: 44 },
            orgItem
          )
        ),
        orgsLength > 5 && React.createElement(
          "div",
          { className: "ShowMore text-center", onClick: function onClick() {
              return _this5.setShowFlag();
            } },
          !this.state.moreFlag ? "Show more" : "Show less"
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      var _this6 = this;

      return React.createElement(
        Nav,
        null,
        getCurrentUser() === null ? React.createElement("div", {
          id: "sokt-sso",
          "data-redirect-uri": process.env.REACT_APP_UI_BASE_URL,
          "data-source": "sokt-app",
          "data-token-key": "sokt_at",
          "data-view": "button"
        }) :
        // <Navbar.Collapse className="justify-content-end">
        React.createElement(
          React.Fragment,
          null,
          React.createElement(
            Dropdown,
            { drop: "down", className: "profile-dropdown transition d-flex align-items-center" },
            React.createElement(Dropdown.Toggle, {
              as: React.forwardRef(function (_ref3, ref1) {
                var children = _ref3.children,
                    onClick = _ref3.onClick;
                return _this6.renderAvatarWithOrg(onClick, ref1);
              }),
              id: "dropdown-custom-components"
            }),
            React.createElement(
              Dropdown.Menu,
              { className: "settings-content" },
              React.createElement(
                "div",
                { className: "text-center org-name" },
                this.getCurrentOrg().name || null
              ),
              React.createElement(Dropdown.Divider, null),
              React.createElement(
                "div",
                {
                  className: "settings-item hover-block pt-0 pb-0",
                  onClick: function onClick() {}
                },
                React.createElement(
                  "div",
                  { className: "settings-label d-flex align-items-center" },
                  React.createElement(
                    "svg",
                    { width: "25", height: "25", viewBox: "0 0 18 18", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
                    React.createElement("path", { d: "M15 15.75V14.25C15 13.4544 14.6839 12.6913 14.1213 12.1287C13.5587 11.5661 12.7956 11.25 12 11.25H6C5.20435 11.25 4.44129 11.5661 3.87868 12.1287C3.31607 12.6913 3 13.4544 3 14.25V15.75", stroke: "#000", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
                    React.createElement("path", { d: "M9 8.25C10.6569 8.25 12 6.90685 12 5.25C12 3.59315 10.6569 2.25 9 2.25C7.34315 2.25 6 3.59315 6 5.25C6 6.90685 7.34315 8.25 9 8.25Z", stroke: "#000", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" })
                  ),
                  React.createElement(
                    "div",
                    { className: "settings-user-name" },
                    React.createElement(
                      "span",
                      { "class": "settings-label" },
                      this.getUserDetails().name
                    ),
                    React.createElement(
                      "span",
                      { "class": "settings-label-light" },
                      this.getUserDetails().email
                    )
                  )
                )
              ),
              React.createElement(
                "div",
                { className: "profile-sm-block" },
                React.createElement(
                  "div",
                  {
                    className: "settings-item",
                    onClick: function onClick() {
                      _this6.openAccountAndSettings();
                    }
                  },
                  React.createElement(
                    Dropdown.Item,
                    null,
                    React.createElement(
                      "span",
                      { className: "settings-label" },
                      React.createElement(
                        "svg",
                        { width: "18", height: "18", viewBox: "0 0 18 18", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
                        React.createElement("path", { d: "M12 15.75V14.25C12 13.4544 11.6839 12.6913 11.1213 12.1287C10.5587 11.5661 9.79565 11.25 9 11.25H3.75C2.95435 11.25 2.19129 11.5661 1.62868 12.1287C1.06607 12.6913 0.75 13.4544 0.75 14.25V15.75", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
                        React.createElement("path", { d: "M6.375 8.25C8.03185 8.25 9.375 6.90686 9.375 5.25C9.375 3.59315 8.03185 2.25 6.375 2.25C4.71815 2.25 3.375 3.59315 3.375 5.25C3.375 6.90686 4.71815 8.25 6.375 8.25Z", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
                        React.createElement("path", { d: "M15 6V10.5", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
                        React.createElement("path", { d: "M17.25 8.25H12.75", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" })
                      ),
                      "Invite Team"
                    )
                  )
                ),
                React.createElement(
                  Dropdown.Item,
                  null,
                  this.renderBilling()
                ),
                React.createElement(
                  Dropdown.Item,
                  null,
                  this.renderSettings()
                ),
                React.createElement(
                  "div",
                  {
                    className: "settings-item",
                    onClick: function onClick() {
                      window.location = "/logout";
                    }
                  },
                  React.createElement(
                    "span",
                    { "class": "settings-label" },
                    React.createElement(
                      "svg",
                      { width: "18", height: "18", viewBox: "0 0 18 18", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
                      React.createElement("path", { d: "M13.7698 4.98047C14.7136 5.92456 15.3563 7.1273 15.6166 8.43661C15.8768 9.74592 15.743 11.103 15.2321 12.3363C14.7211 13.5696 13.8559 14.6236 12.746 15.3652C11.636 16.1068 10.331 16.5027 8.9961 16.5027C7.66117 16.5027 6.35621 16.1068 5.24623 15.3652C4.13624 14.6236 3.27108 13.5696 2.76012 12.3363C2.24916 11.103 2.11536 9.74592 2.37563 8.43661C2.63591 7.1273 3.27856 5.92456 4.22235 4.98047", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" }),
                      React.createElement("path", { d: "M9 1.5V9", stroke: "black", "stroke-width": "1.5", "stroke-linecap": "round", "stroke-linejoin": "round" })
                    ),
                    "Logout"
                  )
                )
              ),
              this.renderOrgList()
            )
          )
        )
        // </Navbar.Collapse>

      );
    }
  }]);

  return UserProfile;
}(Component);

export default connect(mapStateToProps, null)(UserProfile);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9Db21wb25lbnRzL3VzZXJQcm9maWxlLmpzeCJdLCJuYW1lcyI6WyJSZWFjdCIsIkNvbXBvbmVudCIsImdldEN1cnJlbnRVc2VyIiwiTmF2IiwiTmF2RHJvcGRvd24iLCJOYXZiYXIiLCJEcm9wZG93biIsImNvbm5lY3QiLCJBdmF0YXIiLCJsaWdodEFycm93IiwiRml4ZWRTaXplTGlzdCIsIkxpc3QiLCJtYXBTdGF0ZVRvUHJvcHMiLCJzdGF0ZSIsInByb2ZpbGUiLCJVc2VyUHJvZmlsZSIsImZpcnN0TmFtZSIsImxhc3ROYW1lIiwiZW1haWwiLCJvcmdGaWx0ZXIiLCJtb3JlRmxhZyIsInByb3BzIiwiZmlyc3RfbmFtZSIsIm5hbWUiLCJoaXN0b3J5IiwicHVzaCIsInBhdGhuYW1lIiwib3JnYW5pemF0aW9uSWQiLCJzZWFyY2giLCJsb2NhdGlvbiIsInByb2Nlc3MiLCJlbnYiLCJSRUFDVF9BUFBfU09DS0VUX0lDT04iLCJvcmdhbml6YXRpb25zIiwib25DbGljayIsInJlZjEiLCJjdXJyZW50T3JnIiwiZ2V0Q3VycmVudE9yZyIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInJldHVybk5hbWUiLCJsYXN0X25hbWUiLCJvcmdzQXJyYXkiLCJPYmplY3QiLCJ2YWx1ZXMiLCJmaWx0ZXJlZE9yZ3NBcnJheSIsImZpbHRlciIsIm9yZyIsInRvTG93ZXJDYXNlIiwiaW5jbHVkZXMiLCJzb3J0IiwiYSIsImIiLCJzZXRTdGF0ZSIsIm9yZ0NvdW50Iiwic2hvd0ZsYWciLCJpZCIsInBhdGhBcnJheSIsInNwbGl0Iiwib3Jnc0xlbmd0aCIsImtleXMiLCJsZW5ndGgiLCJnZXRBbGxPcmdzIiwib3JnSXRlbSIsImluZGV4Iiwic3R5bGUiLCJpdGVtIiwic3dpdGNoT3JnUm91dGUiLCJzd2l0Y2hfb3JnIiwic2V0T3JnRmlsdGVyIiwidGFyZ2V0IiwidmFsdWUiLCJnZXRJdGVtQ291bnQiLCJzZXRTaG93RmxhZyIsIlJFQUNUX0FQUF9VSV9CQVNFX1VSTCIsImZvcndhcmRSZWYiLCJjaGlsZHJlbiIsInJlbmRlckF2YXRhcldpdGhPcmciLCJnZXRVc2VyRGV0YWlscyIsIm9wZW5BY2NvdW50QW5kU2V0dGluZ3MiLCJyZW5kZXJCaWxsaW5nIiwicmVuZGVyU2V0dGluZ3MiLCJ3aW5kb3ciLCJyZW5kZXJPcmdMaXN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLE9BQU9BLEtBQVAsSUFBZ0JDLFNBQWhCLFFBQWlDLE9BQWpDO0FBQ0EsU0FBU0MsY0FBVCxRQUErQixxQkFBL0I7QUFDQSxTQUFTQyxHQUFULEVBQWNDLFdBQWQsRUFBMkJDLE1BQTNCLEVBQW1DQyxRQUFuQyxRQUFtRCxpQkFBbkQ7QUFDQSxTQUFTQyxPQUFULFFBQXdCLGFBQXhCO0FBQ0EsT0FBT0MsTUFBUCxNQUFtQixjQUFuQjtBQUNBLE9BQU9DLFVBQVAsTUFBdUIsd0NBQXZCO0FBQ0E7QUFDQTtBQUNBLFNBQVNDLGlCQUFpQkMsSUFBMUIsUUFBc0MsY0FBdEM7O0FBRUEsSUFBTUMsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFDQyxLQUFELEVBQVc7QUFDakMsU0FBTztBQUNMQyxhQUFTRCxNQUFNQztBQURWLEdBQVA7QUFHRCxDQUpEOztJQU1NQyxXOzs7Ozs7Ozs7Ozs7OztnTUFDSkYsSyxHQUFRO0FBQ05HLGlCQUFXLEVBREw7QUFFTkMsZ0JBQVUsRUFGSjtBQUdOQyxhQUFPLEVBSEQ7QUFJTkMsaUJBQVcsRUFKTDtBQUtOQyxnQkFBVTtBQUxKLEs7Ozs7O3NDQVFVO0FBQ2hCO0FBQ0EsYUFBTyxNQUFQO0FBQ0Q7OztpQ0FFWTtBQUNYLFVBQUlKLFlBQVksS0FBS0ssS0FBTCxDQUFXUCxPQUFYLENBQW1CUSxVQUFuQixJQUFpQyxFQUFqRDtBQUNBLFVBQUlDLE9BQU9QLFNBQVg7QUFDQSxVQUFJRSxRQUFRLEtBQUtHLEtBQUwsQ0FBV1AsT0FBWCxDQUFtQkksS0FBL0I7QUFDQSxVQUFJSyxTQUFTLEdBQWIsRUFBa0I7QUFDaEIsZUFBT0wsS0FBUDtBQUNELE9BRkQsTUFHSztBQUNILGVBQU9LLElBQVA7QUFDRDtBQUNGOzs7NkNBRXdCO0FBQ3ZCLFdBQUtGLEtBQUwsQ0FBV0csT0FBWCxDQUFtQkMsSUFBbkIsQ0FBd0I7QUFDdEJDLDZCQUFtQixLQUFLTCxLQUFMLENBQVdNLGNBQTlCLFlBRHNCO0FBRXRCQyxnQkFBUSxLQUFLUCxLQUFMLENBQVdRLFFBQVgsQ0FBb0JEO0FBRk4sT0FBeEI7QUFJRDs7O3NDQUVpQjtBQUFBOztBQUNoQixhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVUsZUFBZixFQUErQixTQUFTO0FBQUEsbUJBQU0sT0FBS1AsS0FBTCxDQUFXRyxPQUFYLENBQW1CQyxJQUFuQixDQUF3QjtBQUNwRUMsbUNBQW1CLE9BQUtMLEtBQUwsQ0FBV00sY0FBOUI7QUFEb0UsYUFBeEIsQ0FBTjtBQUFBLFdBQXhDO0FBR0U7QUFBQTtBQUFBLFlBQU0sV0FBVSxnQkFBaEI7QUFDQSx1Q0FBSyxLQUFLRyxRQUFRQyxHQUFSLENBQVlDLHFCQUF0QixFQUE2QyxLQUFJLFdBQWpELEVBQTZELFFBQU8sTUFBcEUsRUFBMkUsT0FBTSxNQUFqRixFQUF3RixXQUFVLE9BQWxHLEdBREE7QUFBQTtBQUFBO0FBSEYsT0FERjtBQVVEOzs7b0NBRWM7QUFBQTs7QUFDYixhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVUsZUFBZixFQUErQixTQUFTO0FBQUEsbUJBQUksT0FBS1gsS0FBTCxDQUFXRyxPQUFYLENBQW1CQyxJQUFuQixDQUF3QjtBQUNsRUMsbUNBQW1CLE9BQUtMLEtBQUwsQ0FBV00sY0FBOUI7QUFEa0UsYUFBeEIsQ0FBSjtBQUFBLFdBQXhDO0FBR0E7QUFBQTtBQUFBLFlBQU0sV0FBVSxnQkFBaEI7QUFDRTtBQUFBO0FBQUEsY0FBSyxPQUFNLElBQVgsRUFBZ0IsUUFBTyxJQUF2QixFQUE0QixTQUFRLFdBQXBDLEVBQWdELE1BQUssTUFBckQsRUFBNEQsT0FBTSw0QkFBbEU7QUFDRSwwQ0FBTSxHQUFFLDJRQUFSLEVBQW9SLFFBQU8sT0FBM1IsRUFBbVMsZ0JBQWEsS0FBaFQsRUFBc1Qsa0JBQWUsT0FBclUsRUFBNlUsbUJBQWdCLE9BQTdWLEdBREY7QUFFRSwwQ0FBTSxHQUFFLG1CQUFSLEVBQTRCLFFBQU8sT0FBbkMsRUFBMkMsZ0JBQWEsS0FBeEQsRUFBOEQsa0JBQWUsT0FBN0UsRUFBcUYsbUJBQWdCLE9BQXJHLEdBRkY7QUFHRSwwQ0FBTSxHQUFFLHN5QkFBUixFQUEreUIsTUFBSyxPQUFwekI7QUFIRixXQURGO0FBQUE7QUFBQTtBQUhBLE9BREY7QUFjRDs7O3FDQUVlO0FBQUE7O0FBQ2QsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFVLGVBQWYsRUFBK0IsU0FBUztBQUFBLG1CQUFJLE9BQUtOLEtBQUwsQ0FBV0csT0FBWCxDQUFtQkMsSUFBbkIsQ0FBd0I7QUFDbEVDLG1DQUFtQixPQUFLTCxLQUFMLENBQVdNLGNBQTlCO0FBRGtFLGFBQXhCLENBQUo7QUFBQSxXQUF4QztBQUdBO0FBQUE7QUFBQSxZQUFNLFdBQVUsZ0JBQWhCO0FBQ0E7QUFBQTtBQUFBLGNBQUssT0FBTSxJQUFYLEVBQWdCLFFBQU8sSUFBdkIsRUFBNEIsU0FBUSxXQUFwQyxFQUFnRCxNQUFLLE1BQXJELEVBQTRELE9BQU0sNEJBQWxFO0FBQ0UsMENBQU0sR0FBRSxxSkFBUixFQUE4SixRQUFPLE9BQXJLLEVBQTZLLGdCQUFhLEtBQTFMLEVBQWdNLGtCQUFlLE9BQS9NLEVBQXVOLG1CQUFnQixPQUF2TyxHQURGO0FBRUUsMENBQU0sR0FBRSw4dUhBQVIsRUFBdXZILFFBQU8sT0FBOXZILEVBQXN3SCxnQkFBYSxLQUFueEgsRUFBeXhILGtCQUFlLE9BQXh5SCxFQUFnekgsbUJBQWdCLE9BQWgwSDtBQUZGLFdBREE7QUFBQTtBQUFBO0FBSEEsT0FERjtBQWFEOzs7b0NBRWM7QUFDYixhQUFPLEtBQUtOLEtBQUwsQ0FBV1ksYUFBWCxDQUF5QixLQUFLWixLQUFMLENBQVdNLGNBQXBDLENBQVA7QUFDRDs7O3dDQUVtQk8sUSxFQUFRQyxJLEVBQUs7QUFDL0IsVUFBTUMsYUFBYSxLQUFLQyxhQUFMLEVBQW5CO0FBQ0EsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFVLGFBQWYsRUFBNkIsU0FBUyxpQkFBQ0MsQ0FBRCxFQUFPO0FBQzNDQSxjQUFFQyxjQUFGO0FBQ0FMLHFCQUFRSSxDQUFSO0FBQ0QsV0FIRDtBQUlFLDRCQUFDLE1BQUQsSUFBUSxPQUFPLFNBQWYsRUFBMEIsTUFBTSxLQUFLRSxVQUFMLEVBQWhDLEVBQW1ELE1BQU0sRUFBekQsRUFBNkQsT0FBTSxNQUFuRSxHQUpGO0FBS0U7QUFBQTtBQUFBLFlBQU0sV0FBVSxlQUFoQjtBQUFpQ0oscUJBQVdiO0FBQTVDLFNBTEY7QUFNRTtBQUNFLGVBQUtZLElBRFA7QUFFRSxlQUFLMUIsVUFGUDtBQUdFLGVBQUksZUFITjtBQUlFLHFCQUFVO0FBSlo7QUFORixPQURGO0FBZUQ7OztxQ0FFZTtBQUNkLFVBQUlPLFlBQVksS0FBS0ssS0FBTCxDQUFXUCxPQUFYLENBQW1CUSxVQUFuQixJQUFpQyxFQUFqRDtBQUNBLFVBQUlMLFdBQVcsS0FBS0ksS0FBTCxDQUFXUCxPQUFYLENBQW1CMkIsU0FBbkIsSUFBZ0MsRUFBL0M7QUFDQSxVQUFJbEIsT0FBT1AsWUFBWSxHQUFaLEdBQWtCQyxRQUE3QjtBQUNBLFVBQUlDLFFBQVEsS0FBS0csS0FBTCxDQUFXUCxPQUFYLENBQW1CSSxLQUEvQjtBQUNBLGFBQU8sRUFBQ0EsWUFBRCxFQUFPSyxVQUFQLEVBQVA7QUFDRDs7O2lDQUVXO0FBQ1YsVUFBTW1CLFlBQVlDLE9BQU9DLE1BQVAsQ0FBYyxLQUFLdkIsS0FBTCxDQUFXWSxhQUFYLElBQTRCLEVBQTFDLENBQWxCO0FBRFUsVUFFRmQsU0FGRSxHQUVZLEtBQUtOLEtBRmpCLENBRUZNLFNBRkU7O0FBR1YsVUFBTTBCLG9CQUFvQkgsVUFBVUksTUFBVixDQUFpQjtBQUFBLGVBQ3pDQyxJQUFJeEIsSUFBSixDQUFTeUIsV0FBVCxHQUF1QkMsUUFBdkIsQ0FBZ0M5QixVQUFVNkIsV0FBVixFQUFoQyxDQUR5QztBQUFBLE9BQWpCLEVBR3pCRSxJQUh5QixDQUdwQixVQUFDQyxDQUFELEVBQUdDLENBQUgsRUFBTztBQUNYLFlBQUdELEVBQUU1QixJQUFGLENBQU95QixXQUFQLEtBQXVCSSxFQUFFN0IsSUFBRixDQUFPeUIsV0FBUCxFQUExQixFQUFnRCxPQUFPLENBQUMsQ0FBUixDQUFoRCxLQUNLLElBQUlHLEVBQUU1QixJQUFGLENBQU95QixXQUFQLEtBQXVCSSxFQUFFN0IsSUFBRixDQUFPeUIsV0FBUCxFQUEzQixFQUFpRCxPQUFPLENBQVAsQ0FBakQsS0FDQSxPQUFPLENBQVA7QUFDTixPQVB5QixDQUExQjs7QUFTQSxhQUFPSCxpQkFBUDtBQUNEOzs7a0NBRVk7QUFDWCxVQUFJMUIsWUFBWSxLQUFLTixLQUFMLENBQVdNLFNBQTNCO0FBQ0EsVUFBSUMsV0FBVyxDQUFDLEtBQUtQLEtBQUwsQ0FBV08sUUFBM0I7QUFDQSxVQUFHLENBQUNBLFFBQUosRUFBYTtBQUNYRCxvQkFBWSxFQUFaO0FBQ0Q7QUFDRCxXQUFLa0MsUUFBTCxDQUFjLEVBQUNsQyxvQkFBRCxFQUFXQyxrQkFBWCxFQUFkO0FBQ0Q7OztpQ0FFWUQsUyxFQUFVO0FBQ3JCLFdBQUtrQyxRQUFMLENBQWMsRUFBRWxDLG9CQUFGLEVBQWQ7QUFDRDs7O2lDQUVZbUMsUSxFQUFTO0FBQ3BCLFVBQUlDLFdBQVcsS0FBSzFDLEtBQUwsQ0FBV08sUUFBMUI7QUFDQSxVQUFHa0MsV0FBUyxDQUFULElBQWMsQ0FBQ0MsUUFBbEIsRUFBMkI7QUFDekIsZUFBTyxDQUFQO0FBQ0QsT0FGRCxNQUVLO0FBQ0gsZUFBT0QsUUFBUDtBQUNEO0FBQ0Y7OzttQ0FFY0UsRSxFQUFHO0FBQ2hCLFVBQUk5QixXQUFTLEVBQWI7QUFDQSxVQUFJK0IsWUFBWSxLQUFLcEMsS0FBTCxDQUFXRyxPQUFYLENBQW1CSyxRQUFuQixDQUE0QkgsUUFBNUIsQ0FBcUNnQyxLQUFyQyxDQUEyQyxHQUEzQyxDQUFoQjtBQUNBLFVBQUlELFVBQVUsQ0FBVixNQUFlLFFBQW5CLEVBQTZCO0FBQzNCL0IsOEJBQW9COEIsRUFBcEI7QUFDRCxPQUZELE1BRU87QUFDTDlCLDhCQUFtQjhCLEVBQW5CO0FBQ0Q7QUFDRCxXQUFLbkMsS0FBTCxDQUFXRyxPQUFYLENBQW1CQyxJQUFuQixDQUF3QjtBQUN0QkM7QUFEc0IsT0FBeEI7QUFHRDs7O29DQUVjO0FBQUE7O0FBQ2IsVUFBTWlDLGFBQWFoQixPQUFPaUIsSUFBUCxDQUFZLEtBQUt2QyxLQUFMLENBQVdZLGFBQVgsSUFBNEIsRUFBeEMsRUFBNEM0QixNQUEvRDtBQUNBLFVBQUloQixvQkFBb0IsS0FBS2lCLFVBQUwsRUFBeEI7QUFDQWpCLDBCQUFrQkEsa0JBQWtCQyxNQUFsQixDQUF5QixVQUFDQyxHQUFEO0FBQUEsZUFBUUEsSUFBSVMsRUFBSixLQUFTLE9BQUtuQyxLQUFMLENBQVdNLGNBQTVCO0FBQUEsT0FBekIsQ0FBbEI7O0FBRUEsVUFBTW9DLFVBQVUsU0FBVkEsT0FBVSxRQUFzQjtBQUFBLFlBQW5CQyxLQUFtQixTQUFuQkEsS0FBbUI7QUFBQSxZQUFaQyxLQUFZLFNBQVpBLEtBQVk7O0FBQ3BDLFlBQU1DLE9BQU9yQixrQkFBa0JtQixLQUFsQixDQUFiO0FBQ0EsZUFDRTtBQUFBO0FBQUE7QUFDRSxpQkFBS0UsS0FBS1YsRUFEWjtBQUVFLHVCQUFVLG1FQUZaO0FBR0UsbUJBQU9TLEtBSFQ7QUFJRSxxQkFBUyxtQkFDUDtBQUNFLHFCQUFLRSxjQUFMLENBQW9CRCxLQUFLVixFQUF6QjtBQUNBLHFCQUFLbkMsS0FBTCxDQUFXK0MsVUFBWCxDQUFzQkYsS0FBS1YsRUFBM0I7QUFDRDtBQVJMO0FBVUk7QUFBQyxvQkFBRCxDQUFVLElBQVY7QUFBQTtBQUNFO0FBQUE7QUFBQSxnQkFBSyxTQUFNLDhCQUFYO0FBQ0U7QUFBQTtBQUFBLGtCQUFLLFdBQVUsc0NBQWY7QUFBdURVLHFCQUFLM0M7QUFBNUQsZUFERjtBQUVFO0FBQUE7QUFBQSxrQkFBSyxXQUFVLFdBQWY7QUFDRTtBQUFBO0FBQUEsb0JBQUssT0FBTSxJQUFYLEVBQWdCLFFBQU8sSUFBdkIsRUFBNEIsU0FBUSxXQUFwQyxFQUFnRCxNQUFLLE1BQXJELEVBQTRELE9BQU0sNEJBQWxFO0FBQ0UsZ0RBQU0sR0FBRSw2QkFBUixFQUFzQyxRQUFPLFNBQTdDLEVBQXVELGdCQUFhLEtBQXBFLEVBQTBFLGtCQUFlLE9BQXpGLEVBQWlHLG1CQUFnQixPQUFqSDtBQURGO0FBREY7QUFGRjtBQURGO0FBVkosU0FERjtBQXdCRCxPQTFCRDs7QUE0QkEsYUFDSW9DLGFBQWEsQ0FBYixJQUFrQjtBQUFBO0FBQUE7QUFDbEI7QUFBQTtBQUFBLFlBQUssV0FBVSw2QkFBZjtBQUFBO0FBQUEsU0FEa0I7QUFFbEI7QUFBQTtBQUFBLFlBQUssV0FBVSw0QkFBZjtBQUNHLGVBQUs5QyxLQUFMLENBQVdPLFFBQVgsSUFDRDtBQUFBO0FBQUE7QUFDRTtBQUNFLHlCQUFVLGNBRFo7QUFFRSx3QkFBVSxrQkFBQ2tCLENBQUQ7QUFBQSx1QkFBSyxPQUFLK0IsWUFBTCxDQUFrQi9CLEVBQUVnQyxNQUFGLENBQVNDLEtBQTNCLEVBQWlDMUIsa0JBQWtCZ0IsTUFBbEIsSUFBMEIsQ0FBM0QsQ0FBTDtBQUFBLGVBRlo7QUFHRSxxQkFBTyxLQUFLaEQsS0FBTCxDQUFXTSxTQUhwQjtBQUlFLDJCQUFZO0FBSmQ7QUFERixXQUZGO0FBV0MwQiw0QkFBa0JnQixNQUFsQixJQUE0QixDQUE1QixHQUNHO0FBQUE7QUFBQSxjQUFLLFdBQVUsd0JBQWY7QUFBd0M7QUFBQTtBQUFBLGdCQUFPLFdBQVUsUUFBakI7QUFBQTtBQUFBO0FBQXhDLFdBREgsR0FFRztBQUFDLGdCQUFEO0FBQUEsY0FBTSxRQUFRaEIsa0JBQWtCZ0IsTUFBbEIsR0FBMkIsQ0FBM0IsR0FBK0IsS0FBR2hCLGtCQUFrQmdCLE1BQXBELEdBQTZELEdBQTNFLEVBQWdGLFdBQVcsS0FBS1csWUFBTCxDQUFrQjNCLGtCQUFrQmdCLE1BQXBDLENBQTNGLEVBQXdJLFVBQVUsRUFBbEo7QUFDR0U7QUFESDtBQWJKLFNBRmtCO0FBbUJuQkoscUJBQWEsQ0FBYixJQUNBO0FBQUE7QUFBQSxZQUFLLFdBQVUsc0JBQWYsRUFBc0MsU0FBUztBQUFBLHFCQUFJLE9BQUtjLFdBQUwsRUFBSjtBQUFBLGFBQS9DO0FBQ0csV0FBQyxLQUFLNUQsS0FBTCxDQUFXTyxRQUFaLEdBQXVCLFdBQXZCLEdBQXFDO0FBRHhDO0FBcEJtQixPQUR0QjtBQTJCRDs7OzZCQUVRO0FBQUE7O0FBQ1AsYUFDRTtBQUFDLFdBQUQ7QUFBQTtBQUNHbEIsNkJBQXFCLElBQXJCLEdBQ0M7QUFDRSxjQUFHLFVBREw7QUFFRSwrQkFBbUI0QixRQUFRQyxHQUFSLENBQVkyQyxxQkFGakM7QUFHRSx5QkFBWSxVQUhkO0FBSUUsNEJBQWUsU0FKakI7QUFLRSx1QkFBVTtBQUxaLFVBREQ7QUFTQztBQUNBO0FBQUMsZUFBRCxDQUFPLFFBQVA7QUFBQTtBQUNFO0FBQUMsb0JBQUQ7QUFBQSxjQUFVLE1BQUssTUFBZixFQUFzQixXQUFVLHVEQUFoQztBQUNFLGdDQUFDLFFBQUQsQ0FBVSxNQUFWO0FBQ0Usa0JBQUkxRSxNQUFNMkUsVUFBTixDQUFpQixpQkFBd0J4QyxJQUF4QjtBQUFBLG9CQUFHeUMsUUFBSCxTQUFHQSxRQUFIO0FBQUEsb0JBQWExQyxPQUFiLFNBQWFBLE9BQWI7QUFBQSx1QkFDbkIsT0FBSzJDLG1CQUFMLENBQXlCM0MsT0FBekIsRUFBaUNDLElBQWpDLENBRG1CO0FBQUEsZUFBakIsQ0FETjtBQUlFLGtCQUFHO0FBSkwsY0FERjtBQU9FO0FBQUMsc0JBQUQsQ0FBVSxJQUFWO0FBQUEsZ0JBQWUsV0FBVSxrQkFBekI7QUFDRTtBQUFBO0FBQUEsa0JBQUssV0FBVSxzQkFBZjtBQUNHLHFCQUFLRSxhQUFMLEdBQXFCZCxJQUFyQixJQUE2QjtBQURoQyxlQURGO0FBSUUsa0NBQUMsUUFBRCxDQUFVLE9BQVYsT0FKRjtBQUtFO0FBQUE7QUFBQTtBQUNFLDZCQUFVLHFDQURaO0FBRUUsMkJBQVMsbUJBQU0sQ0FBRztBQUZwQjtBQUlFO0FBQUE7QUFBQSxvQkFBSyxXQUFVLDBDQUFmO0FBQ0U7QUFBQTtBQUFBLHNCQUFLLE9BQU0sSUFBWCxFQUFnQixRQUFPLElBQXZCLEVBQTRCLFNBQVEsV0FBcEMsRUFBZ0QsTUFBSyxNQUFyRCxFQUE0RCxPQUFNLDRCQUFsRTtBQUNFLGtEQUFNLEdBQUUsMkxBQVIsRUFBb00sUUFBTyxNQUEzTSxFQUFrTixnQkFBYSxLQUEvTixFQUFxTyxrQkFBZSxPQUFwUCxFQUE0UCxtQkFBZ0IsT0FBNVEsR0FERjtBQUVFLGtEQUFNLEdBQUUscUlBQVIsRUFBOEksUUFBTyxNQUFySixFQUE0SixnQkFBYSxLQUF6SyxFQUErSyxrQkFBZSxPQUE5TCxFQUFzTSxtQkFBZ0IsT0FBdE47QUFGRixtQkFERjtBQU1FO0FBQUE7QUFBQSxzQkFBSyxXQUFVLG9CQUFmO0FBQ0U7QUFBQTtBQUFBLHdCQUFNLFNBQU0sZ0JBQVo7QUFBOEIsMkJBQUt1RCxjQUFMLEdBQXNCdkQ7QUFBcEQscUJBREY7QUFFRTtBQUFBO0FBQUEsd0JBQU0sU0FBTSxzQkFBWjtBQUFvQywyQkFBS3VELGNBQUwsR0FBc0I1RDtBQUExRDtBQUZGO0FBTkY7QUFKRixlQUxGO0FBc0JFO0FBQUE7QUFBQSxrQkFBSyxXQUFVLGtCQUFmO0FBUUE7QUFBQTtBQUFBO0FBQ0UsK0JBQVUsZUFEWjtBQUVFLDZCQUFTLG1CQUFNO0FBQUUsNkJBQUs2RCxzQkFBTDtBQUErQjtBQUZsRDtBQUlFO0FBQUMsNEJBQUQsQ0FBVSxJQUFWO0FBQUE7QUFBZTtBQUFBO0FBQUEsd0JBQU0sV0FBVSxnQkFBaEI7QUFDZjtBQUFBO0FBQUEsMEJBQUssT0FBTSxJQUFYLEVBQWdCLFFBQU8sSUFBdkIsRUFBNEIsU0FBUSxXQUFwQyxFQUFnRCxNQUFLLE1BQXJELEVBQTRELE9BQU0sNEJBQWxFO0FBQ0Usc0RBQU0sR0FBRSxtTUFBUixFQUE0TSxRQUFPLE9BQW5OLEVBQTJOLGdCQUFhLEtBQXhPLEVBQThPLGtCQUFlLE9BQTdQLEVBQXFRLG1CQUFnQixPQUFyUixHQURGO0FBRUUsc0RBQU0sR0FBRSxzS0FBUixFQUErSyxRQUFPLE9BQXRMLEVBQThMLGdCQUFhLEtBQTNNLEVBQWlOLGtCQUFlLE9BQWhPLEVBQXdPLG1CQUFnQixPQUF4UCxHQUZGO0FBR0Usc0RBQU0sR0FBRSxZQUFSLEVBQXFCLFFBQU8sT0FBNUIsRUFBb0MsZ0JBQWEsS0FBakQsRUFBdUQsa0JBQWUsT0FBdEUsRUFBOEUsbUJBQWdCLE9BQTlGLEdBSEY7QUFJRSxzREFBTSxHQUFFLG1CQUFSLEVBQTRCLFFBQU8sT0FBbkMsRUFBMkMsZ0JBQWEsS0FBeEQsRUFBOEQsa0JBQWUsT0FBN0UsRUFBcUYsbUJBQWdCLE9BQXJHO0FBSkYsdUJBRGU7QUFPWjtBQVBZO0FBQWY7QUFKRixpQkFSQTtBQXFCQTtBQUFDLDBCQUFELENBQVUsSUFBVjtBQUFBO0FBQWdCLHVCQUFLQyxhQUFMO0FBQWhCLGlCQXJCQTtBQXNCQztBQUFDLDBCQUFELENBQVUsSUFBVjtBQUFBO0FBQWdCLHVCQUFLQyxjQUFMO0FBQWhCLGlCQXRCRDtBQXVCQTtBQUFBO0FBQUE7QUFDRSwrQkFBVSxlQURaO0FBRUUsNkJBQVMsbUJBQU07QUFDYkMsNkJBQU9yRCxRQUFQLEdBQWtCLFNBQWxCO0FBQ0Q7QUFKSDtBQU1FO0FBQUE7QUFBQSxzQkFBTSxTQUFNLGdCQUFaO0FBQ0E7QUFBQTtBQUFBLHdCQUFLLE9BQU0sSUFBWCxFQUFnQixRQUFPLElBQXZCLEVBQTRCLFNBQVEsV0FBcEMsRUFBZ0QsTUFBSyxNQUFyRCxFQUE0RCxPQUFNLDRCQUFsRTtBQUNFLG9EQUFNLEdBQUUseVlBQVIsRUFBa1osUUFBTyxPQUF6WixFQUFpYSxnQkFBYSxLQUE5YSxFQUFvYixrQkFBZSxPQUFuYyxFQUEyYyxtQkFBZ0IsT0FBM2QsR0FERjtBQUVFLG9EQUFNLEdBQUUsVUFBUixFQUFtQixRQUFPLE9BQTFCLEVBQWtDLGdCQUFhLEtBQS9DLEVBQXFELGtCQUFlLE9BQXBFLEVBQTRFLG1CQUFnQixPQUE1RjtBQUZGLHFCQURBO0FBQUE7QUFBQTtBQU5GO0FBdkJBLGVBdEJGO0FBMkRHLG1CQUFLc0QsYUFBTDtBQTNESDtBQVBGO0FBREY7QUFpRkE7O0FBNUZKLE9BREY7QUFpR0Q7Ozs7RUFoVXVCbEYsUzs7QUFtVTFCLGVBQWVNLFFBQVFLLGVBQVIsRUFBeUIsSUFBekIsRUFBK0JHLFdBQS9CLENBQWYiLCJmaWxlIjoidXNlclByb2ZpbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IGdldEN1cnJlbnRVc2VyIH0gZnJvbSBcIi4uL2F1dGgvYXV0aFNlcnZpY2VcIjtcclxuaW1wb3J0IHsgTmF2LCBOYXZEcm9wZG93biwgTmF2YmFyLCBEcm9wZG93biB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xyXG5pbXBvcnQgQXZhdGFyIGZyb20gXCJyZWFjdC1hdmF0YXJcIjtcclxuaW1wb3J0IGxpZ2h0QXJyb3cgZnJvbSAnLi4vYXNzZXRzL2ltYWdlcy9pY29ucy9saWdodC1hcnJvdy5zdmcnXHJcbi8vaW1wb3J0IHsgUFJFVklPVVNfVVNFUiB9IGZyb20gXCIuLi9hY2NvdW50QW5kU2V0dGluZ3MvbG9naW5Bcy9sb2dpbkFzTW9kYWxcIjtcclxuLy8gaW1wb3J0IExvZ2luQXMgZnJvbSBcIi4uL2FjY291bnRBbmRTZXR0aW5ncy9sb2dpbkFzL2xvZ2luQXNcIlxyXG5pbXBvcnQgeyBGaXhlZFNpemVMaXN0IGFzIExpc3QgfSBmcm9tICdyZWFjdC13aW5kb3cnO1xyXG5cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHN0YXRlKSA9PiB7XHJcbiAgcmV0dXJuIHtcclxuICAgIHByb2ZpbGU6IHN0YXRlLnByb2ZpbGUsXHJcbiAgfTtcclxufTtcclxuXHJcbmNsYXNzIFVzZXJQcm9maWxlIGV4dGVuZHMgQ29tcG9uZW50IHtcclxuICBzdGF0ZSA9IHtcclxuICAgIGZpcnN0TmFtZTogXCJcIixcclxuICAgIGxhc3ROYW1lOiBcIlwiLFxyXG4gICAgZW1haWw6IFwiXCIsXHJcbiAgICBvcmdGaWx0ZXI6IFwiXCIsXHJcbiAgICBtb3JlRmxhZzogZmFsc2UsXHJcbiAgfTtcclxuXHJcbiAgZ2V0UHJldmlvdXNVc2VyKCkge1xyXG4gICAgLy8gcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFBSRVZJT1VTX1VTRVIpO1xyXG4gICAgcmV0dXJuIFwiYWJjZFwiO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuTmFtZSgpIHtcclxuICAgIGxldCBmaXJzdE5hbWUgPSB0aGlzLnByb3BzLnByb2ZpbGUuZmlyc3RfbmFtZSB8fCBcIlwiO1xyXG4gICAgbGV0IG5hbWUgPSBmaXJzdE5hbWUgO1xyXG4gICAgbGV0IGVtYWlsID0gdGhpcy5wcm9wcy5wcm9maWxlLmVtYWlsO1xyXG4gICAgaWYgKG5hbWUgPT09IFwiIFwiKSB7XHJcbiAgICAgIHJldHVybiBlbWFpbDtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICByZXR1cm4gbmFtZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9wZW5BY2NvdW50QW5kU2V0dGluZ3MoKSB7XHJcbiAgICB0aGlzLnByb3BzLmhpc3RvcnkucHVzaCh7XHJcbiAgICAgIHBhdGhuYW1lOiBgL29yZ3MvJHt0aGlzLnByb3BzLm9yZ2FuaXphdGlvbklkfS9tYW5hZ2VgLFxyXG4gICAgICBzZWFyY2g6IHRoaXMucHJvcHMubG9jYXRpb24uc2VhcmNoXHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgZXhwbG9yZVByb2R1Y3RzKCkge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJzZXR0aW5ncy1pdGVtXCIgb25DbGljaz17KCkgPT4gdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goe1xyXG4gICAgICAgIHBhdGhuYW1lOiBgL29yZ3MvJHt0aGlzLnByb3BzLm9yZ2FuaXphdGlvbklkfS9wcm9kdWN0c2BcclxuICAgICAgfSl9PlxyXG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInNldHRpbmdzLWxhYmVsXCI+XHJcbiAgICAgICAgPGltZyBzcmM9e3Byb2Nlc3MuZW52LlJFQUNUX0FQUF9TT0NLRVRfSUNPTn0gYWx0PVwic29rdC1pY29uXCIgaGVpZ2h0PVwiMThweFwiIHdpZHRoPVwiMThweFwiIGNsYXNzTmFtZT1cIm1yLTEwXCIvPlxyXG4gICAgICAgIEV4cGxvcmUgUHJvZHVjdHNcclxuICAgICAgICA8L3NwYW4+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG4gIH1cclxuXHJcbiAgcmVuZGVyQmlsbGluZygpe1xyXG4gICAgcmV0dXJuKFxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInNldHRpbmdzLWl0ZW1cIiBvbkNsaWNrPXsoKT0+dGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goe1xyXG4gICAgICAgIHBhdGhuYW1lOiBgL29yZ3MvJHt0aGlzLnByb3BzLm9yZ2FuaXphdGlvbklkfS9iaWxsaW5nYFxyXG4gICAgICB9KX0+XHJcbiAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInNldHRpbmdzLWxhYmVsXCIgPlxyXG4gICAgICAgIDxzdmcgd2lkdGg9XCIxOFwiIGhlaWdodD1cIjE4XCIgdmlld0JveD1cIjAgMCAxOCAxOFwiIGZpbGw9XCJub25lXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPlxyXG4gICAgICAgICAgPHBhdGggZD1cIk05Ljc1IDEuNUg0LjVDNC4xMDIxNyAxLjUgMy43MjA2NCAxLjY1ODA0IDMuNDM5MzQgMS45MzkzNEMzLjE1ODA0IDIuMjIwNjQgMyAyLjYwMjE4IDMgM1YxNUMzIDE1LjM5NzggMy4xNTgwNCAxNS43Nzk0IDMuNDM5MzQgMTYuMDYwN0MzLjcyMDY0IDE2LjM0MiA0LjEwMjE3IDE2LjUgNC41IDE2LjVIMTMuNUMxMy44OTc4IDE2LjUgMTQuMjc5NCAxNi4zNDIgMTQuNTYwNyAxNi4wNjA3QzE0Ljg0MiAxNS43Nzk0IDE1IDE1LjM5NzggMTUgMTVWNi43NUw5Ljc1IDEuNVpcIiBzdHJva2U9XCJibGFja1wiIHN0cm9rZS13aWR0aD1cIjEuNVwiIHN0cm9rZS1saW5lY2FwPVwicm91bmRcIiBzdHJva2UtbGluZWpvaW49XCJyb3VuZFwiLz5cclxuICAgICAgICAgIDxwYXRoIGQ9XCJNOS43NSAxLjVWNi43NUgxNVwiIHN0cm9rZT1cImJsYWNrXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPlxyXG4gICAgICAgICAgPHBhdGggZD1cIk04LjI5ODMgMTQuNzI3M0g4Ljk2ODc1TDguOTc0NDMgMTQuMDcxQzEwLjIwNDUgMTMuOTc3MyAxMC45MTc2IDEzLjMyMzkgMTAuOTIwNSAxMi4zMzgxQzEwLjkxNzYgMTEuMzY5MyAxMC4xODc1IDEwLjg1NTEgOS4xNzYxNCAxMC42Mjc4TDkuMDA4NTIgMTAuNTg4MUw5LjAxOTg5IDkuMTY3NjFDOS4zOTc3MyA5LjI1NTY4IDkuNjI3ODQgOS40OTcxNiA5LjY2MTkzIDkuODU1MTFIMTAuODQwOUMxMC44MjY3IDguOTE0NzcgMTAuMTI1IDguMjQxNDggOS4wMzEyNSA4LjEyMjE2TDkuMDM2OTMgNy40NTQ1NUg4LjM2NjQ4TDguMzYwOCA4LjExNjQ4QzcuMjUgOC4yMjQ0MyA2LjQ2NTkxIDguODk0ODkgNi40NzE1OSA5Ljg2MzY0QzYuNDY4NzUgMTAuNzIxNiA3LjA3Mzg2IDExLjIxMzEgOC4wNTY4MiAxMS40NDg5TDguMzI5NTUgMTEuNTE3TDguMzE1MzQgMTMuMDE5OUM3Ljg1MjI3IDEyLjkzMTggNy41Mzk3NyAxMi42NDc3IDcuNTA4NTIgMTIuMTczM0g2LjMxODE4QzYuMzQ2NTkgMTMuMzIxIDcuMDk5NDMgMTMuOTYzMSA4LjMwMzk4IDE0LjA2ODJMOC4yOTgzIDE0LjcyNzNaTTguOTg1OCAxMy4wMTk5TDguOTk3MTYgMTEuNjkzMkM5LjQzNzUgMTEuODMyNCA5LjY3NjE0IDEyLjAxMTQgOS42Nzg5OCAxMi4zMzUyQzkuNjc2MTQgMTIuNjc5IDkuNDE0NzcgMTIuOTM0NyA4Ljk4NTggMTMuMDE5OVpNOC4zMzgwNyAxMC40MTQ4QzcuOTgyOTUgMTAuMjkyNiA3LjcyNzI3IDEwLjEwOCA3LjczMjk1IDkuNzgxMjVDNy43MzI5NSA5LjQ3NzI3IDcuOTQ4ODYgOS4yNDE0OCA4LjM0OTQzIDkuMTU5MDlMOC4zMzgwNyAxMC40MTQ4WlwiIGZpbGw9XCJibGFja1wiLz5cclxuICAgICAgICA8L3N2Zz5cclxuICAgICAgICBCaWxsaW5nXHJcbiAgICAgIDwvc3Bhbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICApXHJcbiAgfVxyXG5cclxuICByZW5kZXJTZXR0aW5ncygpe1xyXG4gICAgcmV0dXJuKFxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInNldHRpbmdzLWl0ZW1cIiBvbkNsaWNrPXsoKT0+dGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goe1xyXG4gICAgICAgIHBhdGhuYW1lOiBgL29yZ3MvJHt0aGlzLnByb3BzLm9yZ2FuaXphdGlvbklkfS9tYW5hZ2UvYXV0aGtleXNgXHJcbiAgICAgIH0pfT5cclxuICAgICAgPHNwYW4gY2xhc3NOYW1lPVwic2V0dGluZ3MtbGFiZWxcIiA+XHJcbiAgICAgIDxzdmcgd2lkdGg9XCIxOFwiIGhlaWdodD1cIjE4XCIgdmlld0JveD1cIjAgMCAxOCAxOFwiIGZpbGw9XCJub25lXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPlxyXG4gICAgICAgIDxwYXRoIGQ9XCJNOSAxMS4yNUMxMC4yNDI2IDExLjI1IDExLjI1IDEwLjI0MjYgMTEuMjUgOUMxMS4yNSA3Ljc1NzM2IDEwLjI0MjYgNi43NSA5IDYuNzVDNy43NTczNiA2Ljc1IDYuNzUgNy43NTczNiA2Ljc1IDlDNi43NSAxMC4yNDI2IDcuNzU3MzYgMTEuMjUgOSAxMS4yNVpcIiBzdHJva2U9XCJibGFja1wiIHN0cm9rZS13aWR0aD1cIjEuNVwiIHN0cm9rZS1saW5lY2FwPVwicm91bmRcIiBzdHJva2UtbGluZWpvaW49XCJyb3VuZFwiLz5cclxuICAgICAgICA8cGF0aCBkPVwiTTE0LjU1IDExLjI1QzE0LjQ1MDIgMTEuNDc2MiAxNC40MjA0IDExLjcyNzEgMTQuNDY0NSAxMS45NzA0QzE0LjUwODYgMTIuMjEzNyAxNC42MjQ2IDEyLjQzODIgMTQuNzk3NSAxMi42MTVMMTQuODQyNSAxMi42NkMxNC45ODIgMTIuNzk5MyAxNS4wOTI2IDEyLjk2NDcgMTUuMTY4MSAxMy4xNDY4QzE1LjI0MzYgMTMuMzI4OSAxNS4yODI0IDEzLjUyNDEgMTUuMjgyNCAxMy43MjEyQzE1LjI4MjQgMTMuOTE4NCAxNS4yNDM2IDE0LjExMzYgMTUuMTY4MSAxNC4yOTU3QzE1LjA5MjYgMTQuNDc3OCAxNC45ODIgMTQuNjQzMiAxNC44NDI1IDE0Ljc4MjVDMTQuNzAzMiAxNC45MjIgMTQuNTM3OCAxNS4wMzI2IDE0LjM1NTcgMTUuMTA4MUMxNC4xNzM2IDE1LjE4MzYgMTMuOTc4NCAxNS4yMjI0IDEzLjc4MTIgMTUuMjIyNEMxMy41ODQxIDE1LjIyMjQgMTMuMzg4OSAxNS4xODM2IDEzLjIwNjggMTUuMTA4MUMxMy4wMjQ3IDE1LjAzMjYgMTIuODU5MyAxNC45MjIgMTIuNzIgMTQuNzgyNUwxMi42NzUgMTQuNzM3NUMxMi40OTgyIDE0LjU2NDYgMTIuMjczNyAxNC40NDg2IDEyLjAzMDQgMTQuNDA0NUMxMS43ODcxIDE0LjM2MDQgMTEuNTM2MiAxNC4zOTAyIDExLjMxIDE0LjQ5QzExLjA4ODIgMTQuNTg1MSAxMC44OTkgMTQuNzQyOSAxMC43NjU3IDE0Ljk0NDFDMTAuNjMyNSAxNS4xNDU0IDEwLjU2MSAxNS4zODEyIDEwLjU2IDE1LjYyMjVWMTUuNzVDMTAuNTYgMTYuMTQ3OCAxMC40MDIgMTYuNTI5NCAxMC4xMjA3IDE2LjgxMDdDOS44MzkzNSAxNy4wOTIgOS40NTc4MiAxNy4yNSA5LjA2IDE3LjI1QzguNjYyMTcgMTcuMjUgOC4yODA2NCAxNy4wOTIgNy45OTkzNCAxNi44MTA3QzcuNzE4MDMgMTYuNTI5NCA3LjU2IDE2LjE0NzggNy41NiAxNS43NVYxNS42ODI1QzcuNTU0MTkgMTUuNDM0MyA3LjQ3Mzg0IDE1LjE5MzUgNy4zMjkzOCAxNC45OTE1QzcuMTg0OTMgMTQuNzg5NiA2Ljk4MzA1IDE0LjYzNTcgNi43NSAxNC41NUM2LjUyMzc5IDE0LjQ1MDIgNi4yNzI4NSAxNC40MjA0IDYuMDI5NTYgMTQuNDY0NUM1Ljc4NjI2IDE0LjUwODYgNS41NjE3NiAxNC42MjQ2IDUuMzg1IDE0Ljc5NzVMNS4zNCAxNC44NDI1QzUuMjAwNjkgMTQuOTgyIDUuMDM1MjYgMTUuMDkyNiA0Ljg1MzE2IDE1LjE2ODFDNC42NzEwNiAxNS4yNDM2IDQuNDc1ODcgMTUuMjgyNCA0LjI3ODc1IDE1LjI4MjRDNC4wODE2MyAxNS4yODI0IDMuODg2NDQgMTUuMjQzNiAzLjcwNDM0IDE1LjE2ODFDMy41MjIyNCAxNS4wOTI2IDMuMzU2ODEgMTQuOTgyIDMuMjE3NSAxNC44NDI1QzMuMDc4MDMgMTQuNzAzMiAyLjk2NzQgMTQuNTM3OCAyLjg5MTkxIDE0LjM1NTdDMi44MTY0MiAxNC4xNzM2IDIuNzc3NTcgMTMuOTc4NCAyLjc3NzU3IDEzLjc4MTJDMi43Nzc1NyAxMy41ODQxIDIuODE2NDIgMTMuMzg4OSAyLjg5MTkxIDEzLjIwNjhDMi45Njc0IDEzLjAyNDcgMy4wNzgwMyAxMi44NTkzIDMuMjE3NSAxMi43MkwzLjI2MjUgMTIuNjc1QzMuNDM1NCAxMi40OTgyIDMuNTUxMzkgMTIuMjczNyAzLjU5NTUgMTIuMDMwNEMzLjYzOTYyIDExLjc4NzEgMy42MDk4NCAxMS41MzYyIDMuNTEgMTEuMzFDMy40MTQ5MyAxMS4wODgyIDMuMjU3MDcgMTAuODk5IDMuMDU1ODUgMTAuNzY1N0MyLjg1NDYzIDEwLjYzMjUgMi42MTg4NCAxMC41NjEgMi4zNzc1IDEwLjU2SDIuMjVDMS44NTIxNyAxMC41NiAxLjQ3MDY0IDEwLjQwMiAxLjE4OTM0IDEwLjEyMDdDMC45MDgwMzUgOS44MzkzNSAwLjc1IDkuNDU3ODIgMC43NSA5LjA2QzAuNzUgOC42NjIxNyAwLjkwODAzNSA4LjI4MDY0IDEuMTg5MzQgNy45OTkzNEMxLjQ3MDY0IDcuNzE4MDMgMS44NTIxNyA3LjU2IDIuMjUgNy41NkgyLjMxNzVDMi41NjU3NSA3LjU1NDE5IDIuODA2NSA3LjQ3Mzg0IDMuMDA4NDcgNy4zMjkzOEMzLjIxMDQ0IDcuMTg0OTMgMy4zNjQyOSA2Ljk4MzA1IDMuNDUgNi43NUMzLjU0OTg0IDYuNTIzNzkgMy41Nzk2MiA2LjI3Mjg1IDMuNTM1NSA2LjAyOTU2QzMuNDkxMzkgNS43ODYyNiAzLjM3NTQgNS41NjE3NiAzLjIwMjUgNS4zODVMMy4xNTc1IDUuMzRDMy4wMTgwMyA1LjIwMDY5IDIuOTA3NCA1LjAzNTI2IDIuODMxOTEgNC44NTMxNkMyLjc1NjQyIDQuNjcxMDYgMi43MTc1NyA0LjQ3NTg3IDIuNzE3NTcgNC4yNzg3NUMyLjcxNzU3IDQuMDgxNjMgMi43NTY0MiAzLjg4NjQ0IDIuODMxOTEgMy43MDQzNEMyLjkwNzQgMy41MjIyNCAzLjAxODAzIDMuMzU2ODEgMy4xNTc1IDMuMjE3NUMzLjI5NjgxIDMuMDc4MDMgMy40NjIyNCAyLjk2NzQgMy42NDQzNCAyLjg5MTkxQzMuODI2NDQgMi44MTY0MiA0LjAyMTYzIDIuNzc3NTcgNC4yMTg3NSAyLjc3NzU3QzQuNDE1ODcgMi43Nzc1NyA0LjYxMTA2IDIuODE2NDIgNC43OTMxNiAyLjg5MTkxQzQuOTc1MjYgMi45Njc0IDUuMTQwNjkgMy4wNzgwMyA1LjI4IDMuMjE3NUw1LjMyNSAzLjI2MjVDNS41MDE3NiAzLjQzNTQgNS43MjYyNiAzLjU1MTM5IDUuOTY5NTYgMy41OTU1QzYuMjEyODUgMy42Mzk2MiA2LjQ2Mzc5IDMuNjA5ODQgNi42OSAzLjUxSDYuNzVDNi45NzE4MyAzLjQxNDkzIDcuMTYxMDEgMy4yNTcwNyA3LjI5NDI3IDMuMDU1ODVDNy40Mjc1MiAyLjg1NDYzIDcuNDk5MDQgMi42MTg4NCA3LjUgMi4zNzc1VjIuMjVDNy41IDEuODUyMTcgNy42NTgwMyAxLjQ3MDY0IDcuOTM5MzQgMS4xODkzNEM4LjIyMDY0IDAuOTA4MDM1IDguNjAyMTcgMC43NSA5IDAuNzVDOS4zOTc4MiAwLjc1IDkuNzc5MzUgMC45MDgwMzUgMTAuMDYwNyAxLjE4OTM0QzEwLjM0MiAxLjQ3MDY0IDEwLjUgMS44NTIxNyAxMC41IDIuMjVWMi4zMTc1QzEwLjUwMSAyLjU1ODg0IDEwLjU3MjUgMi43OTQ2MyAxMC43MDU3IDIuOTk1ODVDMTAuODM5IDMuMTk3MDcgMTEuMDI4MiAzLjM1NDkzIDExLjI1IDMuNDVDMTEuNDc2MiAzLjU0OTg0IDExLjcyNzEgMy41Nzk2MiAxMS45NzA0IDMuNTM1NUMxMi4yMTM3IDMuNDkxMzkgMTIuNDM4MiAzLjM3NTQgMTIuNjE1IDMuMjAyNUwxMi42NiAzLjE1NzVDMTIuNzk5MyAzLjAxODAzIDEyLjk2NDcgMi45MDc0IDEzLjE0NjggMi44MzE5MUMxMy4zMjg5IDIuNzU2NDIgMTMuNTI0MSAyLjcxNzU3IDEzLjcyMTIgMi43MTc1N0MxMy45MTg0IDIuNzE3NTcgMTQuMTEzNiAyLjc1NjQyIDE0LjI5NTcgMi44MzE5MUMxNC40Nzc4IDIuOTA3NCAxNC42NDMyIDMuMDE4MDMgMTQuNzgyNSAzLjE1NzVDMTQuOTIyIDMuMjk2ODEgMTUuMDMyNiAzLjQ2MjI0IDE1LjEwODEgMy42NDQzNEMxNS4xODM2IDMuODI2NDQgMTUuMjIyNCA0LjAyMTYzIDE1LjIyMjQgNC4yMTg3NUMxNS4yMjI0IDQuNDE1ODcgMTUuMTgzNiA0LjYxMTA2IDE1LjEwODEgNC43OTMxNkMxNS4wMzI2IDQuOTc1MjYgMTQuOTIyIDUuMTQwNjkgMTQuNzgyNSA1LjI4TDE0LjczNzUgNS4zMjVDMTQuNTY0NiA1LjUwMTc2IDE0LjQ0ODYgNS43MjYyNiAxNC40MDQ1IDUuOTY5NTZDMTQuMzYwNCA2LjIxMjg1IDE0LjM5MDIgNi40NjM3OSAxNC40OSA2LjY5VjYuNzVDMTQuNTg1MSA2Ljk3MTgzIDE0Ljc0MjkgNy4xNjEwMSAxNC45NDQxIDcuMjk0MjdDMTUuMTQ1NCA3LjQyNzUyIDE1LjM4MTIgNy40OTkwNCAxNS42MjI1IDcuNUgxNS43NUMxNi4xNDc4IDcuNSAxNi41Mjk0IDcuNjU4MDMgMTYuODEwNyA3LjkzOTM0QzE3LjA5MiA4LjIyMDY0IDE3LjI1IDguNjAyMTcgMTcuMjUgOUMxNy4yNSA5LjM5NzgyIDE3LjA5MiA5Ljc3OTM1IDE2LjgxMDcgMTAuMDYwN0MxNi41Mjk0IDEwLjM0MiAxNi4xNDc4IDEwLjUgMTUuNzUgMTAuNUgxNS42ODI1QzE1LjQ0MTIgMTAuNTAxIDE1LjIwNTQgMTAuNTcyNSAxNS4wMDQxIDEwLjcwNTdDMTQuODAyOSAxMC44MzkgMTQuNjQ1MSAxMS4wMjgyIDE0LjU1IDExLjI1VjExLjI1WlwiIHN0cm9rZT1cImJsYWNrXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPlxyXG4gICAgICA8L3N2Zz5cclxuICAgICAgICBBdXRoIEtleXNcclxuICAgICAgPC9zcGFuPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIClcclxuICB9XHJcblxyXG4gIGdldEN1cnJlbnRPcmcoKXtcclxuICAgIHJldHVybiB0aGlzLnByb3BzLm9yZ2FuaXphdGlvbnNbdGhpcy5wcm9wcy5vcmdhbml6YXRpb25JZF1cclxuICB9XHJcblxyXG4gIHJlbmRlckF2YXRhcldpdGhPcmcob25DbGljayxyZWYxKXtcclxuICAgIGNvbnN0IGN1cnJlbnRPcmcgPSB0aGlzLmdldEN1cnJlbnRPcmcoKTtcclxuICAgIHJldHVybihcclxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9maWxlLWJveFwiIG9uQ2xpY2s9eyhlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIG9uQ2xpY2soZSk7XHJcbiAgICAgIH19PlxyXG4gICAgICAgIDxBdmF0YXIgY29sb3I9eycjMzQzYTQwJ30gbmFtZT17dGhpcy5yZXR1cm5OYW1lKCl9IHNpemU9ezI0fSByb3VuZD1cIjMwcHhcIiAvPlxyXG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInB4LTMgb3JnLW5hbWVcIj57Y3VycmVudE9yZy5uYW1lfTwvc3Bhbj5cclxuICAgICAgICA8aW1nXHJcbiAgICAgICAgICByZWY9e3JlZjF9XHJcbiAgICAgICAgICBzcmM9e2xpZ2h0QXJyb3d9XHJcbiAgICAgICAgICBhbHQ9XCJzZXR0aW5ncy1nZWFyXCJcclxuICAgICAgICAgIGNsYXNzTmFtZT1cInRyYW5zaXRpb25cIlxyXG4gICAgICAgIC8+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG4gIH1cclxuXHJcbiAgZ2V0VXNlckRldGFpbHMoKXtcclxuICAgIGxldCBmaXJzdE5hbWUgPSB0aGlzLnByb3BzLnByb2ZpbGUuZmlyc3RfbmFtZSB8fCBcIlwiO1xyXG4gICAgbGV0IGxhc3ROYW1lID0gdGhpcy5wcm9wcy5wcm9maWxlLmxhc3RfbmFtZSB8fCBcIlwiO1xyXG4gICAgbGV0IG5hbWUgPSBmaXJzdE5hbWUgKyBcIiBcIiArIGxhc3ROYW1lO1xyXG4gICAgbGV0IGVtYWlsID0gdGhpcy5wcm9wcy5wcm9maWxlLmVtYWlsO1xyXG4gICAgcmV0dXJuIHtlbWFpbCxuYW1lfVxyXG4gIH1cclxuXHJcbiAgZ2V0QWxsT3Jncygpe1xyXG4gICAgY29uc3Qgb3Jnc0FycmF5ID0gT2JqZWN0LnZhbHVlcyh0aGlzLnByb3BzLm9yZ2FuaXphdGlvbnMgfHwge30pXHJcbiAgICBjb25zdCB7IG9yZ0ZpbHRlciB9ID0gdGhpcy5zdGF0ZVxyXG4gICAgY29uc3QgZmlsdGVyZWRPcmdzQXJyYXkgPSBvcmdzQXJyYXkuZmlsdGVyKG9yZyA9PiBcclxuICAgICAgb3JnLm5hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhvcmdGaWx0ZXIudG9Mb3dlckNhc2UoKVxyXG4gICAgKSlcclxuICAgIC5zb3J0KChhLGIpPT57XHJcbiAgICAgIGlmKGEubmFtZS50b0xvd2VyQ2FzZSgpIDwgYi5uYW1lLnRvTG93ZXJDYXNlKCkpIHJldHVybiAtMTtcclxuICAgICAgZWxzZSBpZiAoYS5uYW1lLnRvTG93ZXJDYXNlKCkgPiBiLm5hbWUudG9Mb3dlckNhc2UoKSkgcmV0dXJuIDE7XHJcbiAgICAgIGVsc2UgcmV0dXJuIDA7XHJcbiAgICB9KVxyXG5cclxuICAgIHJldHVybiBmaWx0ZXJlZE9yZ3NBcnJheVxyXG4gIH1cclxuXHJcbiAgc2V0U2hvd0ZsYWcoKXtcclxuICAgIGxldCBvcmdGaWx0ZXIgPSB0aGlzLnN0YXRlLm9yZ0ZpbHRlclxyXG4gICAgbGV0IG1vcmVGbGFnID0gIXRoaXMuc3RhdGUubW9yZUZsYWc7XHJcbiAgICBpZighbW9yZUZsYWcpe1xyXG4gICAgICBvcmdGaWx0ZXIgPSBcIlwiXHJcbiAgICB9XHJcbiAgICB0aGlzLnNldFN0YXRlKHtvcmdGaWx0ZXIsbW9yZUZsYWd9KVxyXG4gIH1cclxuXHJcbiAgc2V0T3JnRmlsdGVyKG9yZ0ZpbHRlcil7XHJcbiAgICB0aGlzLnNldFN0YXRlKHsgb3JnRmlsdGVyfSlcclxuICB9XHJcblxyXG4gIGdldEl0ZW1Db3VudChvcmdDb3VudCl7XHJcbiAgICBsZXQgc2hvd0ZsYWcgPSB0aGlzLnN0YXRlLm1vcmVGbGFnO1xyXG4gICAgaWYob3JnQ291bnQ+NSAmJiAhc2hvd0ZsYWcpe1xyXG4gICAgICByZXR1cm4gNTtcclxuICAgIH1lbHNle1xyXG4gICAgICByZXR1cm4gb3JnQ291bnQ7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzd2l0Y2hPcmdSb3V0ZShpZCl7XHJcbiAgICBsZXQgcGF0aG5hbWU9XCJcIjtcclxuICAgIGxldCBwYXRoQXJyYXkgPSB0aGlzLnByb3BzLmhpc3RvcnkubG9jYXRpb24ucGF0aG5hbWUuc3BsaXQoJy8nKTtcclxuICAgIGlmIChwYXRoQXJyYXlbM109PT0nbWFuYWdlJykge1xyXG4gICAgICBwYXRobmFtZSA9IGAvb3Jncy8ke2lkfS9tYW5hZ2VgXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBwYXRobmFtZT0gYC9vcmdzLyR7aWR9YDtcclxuICAgIH1cclxuICAgIHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKHtcclxuICAgICAgcGF0aG5hbWVcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmVuZGVyT3JnTGlzdCgpe1xyXG4gICAgY29uc3Qgb3Jnc0xlbmd0aCA9IE9iamVjdC5rZXlzKHRoaXMucHJvcHMub3JnYW5pemF0aW9ucyB8fCB7fSkubGVuZ3RoO1xyXG4gICAgbGV0IGZpbHRlcmVkT3Jnc0FycmF5ID0gdGhpcy5nZXRBbGxPcmdzKCk7XHJcbiAgICBmaWx0ZXJlZE9yZ3NBcnJheT1maWx0ZXJlZE9yZ3NBcnJheS5maWx0ZXIoKG9yZyk9PiBvcmcuaWQhPT10aGlzLnByb3BzLm9yZ2FuaXphdGlvbklkKVxyXG5cclxuICAgIGNvbnN0IG9yZ0l0ZW0gPSAoeyBpbmRleCwgc3R5bGUgfSkgPT4ge1xyXG4gICAgICBjb25zdCBpdGVtID0gZmlsdGVyZWRPcmdzQXJyYXlbaW5kZXhdO1xyXG4gICAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgXHJcbiAgICAgICAgICBrZXk9e2l0ZW0uaWR9IFxyXG4gICAgICAgICAgY2xhc3NOYW1lPSdkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyIHAtMiBzZXR0aW5ncy1pdGVtIGp1c3RpZnktc3BhY2UtYmV0d2VlbicgXHJcbiAgICAgICAgICBzdHlsZT17c3R5bGV9IFxyXG4gICAgICAgICAgb25DbGljaz17KCk9PlxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgdGhpcy5zd2l0Y2hPcmdSb3V0ZShpdGVtLmlkKVxyXG4gICAgICAgICAgICAgIHRoaXMucHJvcHMuc3dpdGNoX29yZyhpdGVtLmlkKVxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgICAgPERyb3Bkb3duLkl0ZW0+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImQtZmxleCBqdXN0aWZ5LXNwYWNlLWJldHdlZW5cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdib2R5LTMgdGV4dC1ibGFjayB0ZXh0LXRydW5jYXRlIHBsLTInPntpdGVtLm5hbWV9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nb3Jncy1pY29uJz5cclxuICAgICAgICAgICAgICAgICAgPHN2ZyB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMThcIiB2aWV3Qm94PVwiMCAwIDE4IDE4XCIgZmlsbD1cIm5vbmVcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD1cIk02Ljc1IDEzLjVMMTEuMjUgOUw2Ljc1IDQuNVwiIHN0cm9rZT1cIiM0RjRGNEZcIiBzdHJva2Utd2lkdGg9XCIxLjVcIiBzdHJva2UtbGluZWNhcD1cInJvdW5kXCIgc3Ryb2tlLWxpbmVqb2luPVwicm91bmRcIi8+XHJcbiAgICAgICAgICAgICAgICAgIDwvc3ZnPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvRHJvcGRvd24uSXRlbT5cclxuICAgICAgICA8L2Rpdj4gIFxyXG4gICAgICAgIFxyXG4gICAgICApXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuKFxyXG4gICAgICAoIG9yZ3NMZW5ndGggPiAxICYmIDxkaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LXVwcGVyY2FzZSB0ZXh0LXNtLWJvbGRcIj5TV0lUQ0ggT1JHUzwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdvcmdzLWxpc3QgcHJvZmlsZS1zbS1ibG9jayc+XHJcbiAgICAgICAgICB7dGhpcy5zdGF0ZS5tb3JlRmxhZyAmJiBcclxuICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxpbnB1dCBcclxuICAgICAgICAgICAgICBjbGFzc05hbWU9J2Zvcm0tY29udHJvbCdcclxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpPT50aGlzLnNldE9yZ0ZpbHRlcihlLnRhcmdldC52YWx1ZSxmaWx0ZXJlZE9yZ3NBcnJheS5sZW5ndGh8fDApfVxyXG4gICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm9yZ0ZpbHRlcn1cclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj0nU2VhcmNoJ1xyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAge2ZpbHRlcmVkT3Jnc0FycmF5Lmxlbmd0aCA9PSAwIFxyXG4gICAgICAgICAgPyA8ZGl2IGNsYXNzTmFtZT0ncGItMiB0ZXh0LWNlbnRlciB3LTEwMCc+PHNtYWxsIGNsYXNzTmFtZT0nYm9keS02Jz5ObyBPcmdhbml6YXRpb25zIEZvdW5kPC9zbWFsbD48L2Rpdj5cclxuICAgICAgICAgIDogPExpc3QgaGVpZ2h0PXtmaWx0ZXJlZE9yZ3NBcnJheS5sZW5ndGggPCA1ID8gMzYqZmlsdGVyZWRPcmdzQXJyYXkubGVuZ3RoIDogMjIwfSBpdGVtQ291bnQ9e3RoaXMuZ2V0SXRlbUNvdW50KGZpbHRlcmVkT3Jnc0FycmF5Lmxlbmd0aCl9IGl0ZW1TaXplPXs0NH0+XHJcbiAgICAgICAgICAgICAge29yZ0l0ZW19XHJcbiAgICAgICAgICAgIDwvTGlzdD59XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICB7b3Jnc0xlbmd0aCA+IDUgJiZcclxuICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiU2hvd01vcmUgdGV4dC1jZW50ZXJcIiBvbkNsaWNrPXsoKT0+dGhpcy5zZXRTaG93RmxhZygpfT5cclxuICAgICAgICAgeyF0aGlzLnN0YXRlLm1vcmVGbGFnID8gXCJTaG93IG1vcmVcIiA6IFwiU2hvdyBsZXNzXCJ9XHJcbiAgICAgICA8L2Rpdj59XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICApXHJcbiAgICApXHJcbiAgfVxyXG5cclxuICByZW5kZXIoKSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8TmF2PlxyXG4gICAgICAgIHtnZXRDdXJyZW50VXNlcigpID09PSBudWxsID8gKFxyXG4gICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBpZD1cInNva3Qtc3NvXCJcclxuICAgICAgICAgICAgZGF0YS1yZWRpcmVjdC11cmk9e3Byb2Nlc3MuZW52LlJFQUNUX0FQUF9VSV9CQVNFX1VSTH1cclxuICAgICAgICAgICAgZGF0YS1zb3VyY2U9XCJzb2t0LWFwcFwiXHJcbiAgICAgICAgICAgIGRhdGEtdG9rZW4ta2V5PVwic29rdF9hdFwiXHJcbiAgICAgICAgICAgIGRhdGEtdmlldz1cImJ1dHRvblwiXHJcbiAgICAgICAgICA+PC9kaXY+XHJcbiAgICAgICAgKSA6IChcclxuICAgICAgICAgIC8vIDxOYXZiYXIuQ29sbGFwc2UgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWVuZFwiPlxyXG4gICAgICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgICAgICA8RHJvcGRvd24gZHJvcD1cImRvd25cIiBjbGFzc05hbWU9XCJwcm9maWxlLWRyb3Bkb3duIHRyYW5zaXRpb24gZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgIDxEcm9wZG93bi5Ub2dnbGVcclxuICAgICAgICAgICAgICAgIGFzPXtSZWFjdC5mb3J3YXJkUmVmKCh7IGNoaWxkcmVuLCBvbkNsaWNrIH0sIHJlZjEpID0+IChcclxuICAgICAgICAgICAgICAgICAgdGhpcy5yZW5kZXJBdmF0YXJXaXRoT3JnKG9uQ2xpY2sscmVmMSlcclxuICAgICAgICAgICAgICAgICkpfVxyXG4gICAgICAgICAgICAgICAgaWQ9XCJkcm9wZG93bi1jdXN0b20tY29tcG9uZW50c1wiXHJcbiAgICAgICAgICAgICAgPjwvRHJvcGRvd24uVG9nZ2xlPlxyXG4gICAgICAgICAgICAgIDxEcm9wZG93bi5NZW51IGNsYXNzTmFtZT1cInNldHRpbmdzLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dC1jZW50ZXIgb3JnLW5hbWVcIj5cclxuICAgICAgICAgICAgICAgICAge3RoaXMuZ2V0Q3VycmVudE9yZygpLm5hbWUgfHwgbnVsbH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPERyb3Bkb3duLkRpdmlkZXIgLz5cclxuICAgICAgICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2V0dGluZ3MtaXRlbSBob3Zlci1ibG9jayBwdC0wIHBiLTBcIlxyXG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7IH19XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2V0dGluZ3MtbGFiZWwgZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzdmcgd2lkdGg9XCIyNVwiIGhlaWdodD1cIjI1XCIgdmlld0JveD1cIjAgMCAxOCAxOFwiIGZpbGw9XCJub25lXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHBhdGggZD1cIk0xNSAxNS43NVYxNC4yNUMxNSAxMy40NTQ0IDE0LjY4MzkgMTIuNjkxMyAxNC4xMjEzIDEyLjEyODdDMTMuNTU4NyAxMS41NjYxIDEyLjc5NTYgMTEuMjUgMTIgMTEuMjVINkM1LjIwNDM1IDExLjI1IDQuNDQxMjkgMTEuNTY2MSAzLjg3ODY4IDEyLjEyODdDMy4zMTYwNyAxMi42OTEzIDMgMTMuNDU0NCAzIDE0LjI1VjE1Ljc1XCIgc3Ryb2tlPVwiIzAwMFwiIHN0cm9rZS13aWR0aD1cIjEuNVwiIHN0cm9rZS1saW5lY2FwPVwicm91bmRcIiBzdHJva2UtbGluZWpvaW49XCJyb3VuZFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPVwiTTkgOC4yNUMxMC42NTY5IDguMjUgMTIgNi45MDY4NSAxMiA1LjI1QzEyIDMuNTkzMTUgMTAuNjU2OSAyLjI1IDkgMi4yNUM3LjM0MzE1IDIuMjUgNiAzLjU5MzE1IDYgNS4yNUM2IDYuOTA2ODUgNy4zNDMxNSA4LjI1IDkgOC4yNVpcIiBzdHJva2U9XCIjMDAwXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L3N2Zz5cclxuICAgICAgICAgICAgICAgICAgICB7Lyoge3RoaXMucHJvcHMucHJvZmlsZS5lbWFpbCB8fCBcIlwifSAqL31cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNldHRpbmdzLXVzZXItbmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJzZXR0aW5ncy1sYWJlbFwiPnt0aGlzLmdldFVzZXJEZXRhaWxzKCkubmFtZX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cInNldHRpbmdzLWxhYmVsLWxpZ2h0XCI+e3RoaXMuZ2V0VXNlckRldGFpbHMoKS5lbWFpbH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9maWxlLXNtLWJsb2NrXCI+XHJcbiAgICAgICAgICAgICAgICB7Lyogeyh0aGlzLnByb3BzLmFjY2Vzc0xldmVsLmlzX3N1cGVyX2FkbWluIHx8IHRoaXMuZ2V0UHJldmlvdXNVc2VyKCkpICYmPGRpdlxyXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzZXR0aW5ncy1pdGVtIHAtMFwiXHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgIDxMb2dpbkFzIHsuLi50aGlzLnByb3BzfT48L0xvZ2luQXM+XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj59ICovfVxyXG4gICAgICAgICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzZXR0aW5ncy1pdGVtXCJcclxuICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4geyB0aGlzLm9wZW5BY2NvdW50QW5kU2V0dGluZ3MoKSB9fVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICA8RHJvcGRvd24uSXRlbT48c3BhbiBjbGFzc05hbWU9XCJzZXR0aW5ncy1sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICA8c3ZnIHdpZHRoPVwiMThcIiBoZWlnaHQ9XCIxOFwiIHZpZXdCb3g9XCIwIDAgMTggMThcIiBmaWxsPVwibm9uZVwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPVwiTTEyIDE1Ljc1VjE0LjI1QzEyIDEzLjQ1NDQgMTEuNjgzOSAxMi42OTEzIDExLjEyMTMgMTIuMTI4N0MxMC41NTg3IDExLjU2NjEgOS43OTU2NSAxMS4yNSA5IDExLjI1SDMuNzVDMi45NTQzNSAxMS4yNSAyLjE5MTI5IDExLjU2NjEgMS42Mjg2OCAxMi4xMjg3QzEuMDY2MDcgMTIuNjkxMyAwLjc1IDEzLjQ1NDQgMC43NSAxNC4yNVYxNS43NVwiIHN0cm9rZT1cImJsYWNrXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9XCJNNi4zNzUgOC4yNUM4LjAzMTg1IDguMjUgOS4zNzUgNi45MDY4NiA5LjM3NSA1LjI1QzkuMzc1IDMuNTkzMTUgOC4wMzE4NSAyLjI1IDYuMzc1IDIuMjVDNC43MTgxNSAyLjI1IDMuMzc1IDMuNTkzMTUgMy4zNzUgNS4yNUMzLjM3NSA2LjkwNjg2IDQuNzE4MTUgOC4yNSA2LjM3NSA4LjI1WlwiIHN0cm9rZT1cImJsYWNrXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9XCJNMTUgNlYxMC41XCIgc3Ryb2tlPVwiYmxhY2tcIiBzdHJva2Utd2lkdGg9XCIxLjVcIiBzdHJva2UtbGluZWNhcD1cInJvdW5kXCIgc3Ryb2tlLWxpbmVqb2luPVwicm91bmRcIi8+XHJcbiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD1cIk0xNy4yNSA4LjI1SDEyLjc1XCIgc3Ryb2tlPVwiYmxhY2tcIiBzdHJva2Utd2lkdGg9XCIxLjVcIiBzdHJva2UtbGluZWNhcD1cInJvdW5kXCIgc3Ryb2tlLWxpbmVqb2luPVwicm91bmRcIi8+XHJcbiAgICAgICAgICAgICAgICAgIDwvc3ZnPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcIkludml0ZSBUZWFtXCJ9PC9zcGFuPjwvRHJvcGRvd24uSXRlbT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPERyb3Bkb3duLkl0ZW0+e3RoaXMucmVuZGVyQmlsbGluZygpfTwvRHJvcGRvd24uSXRlbT5cclxuICAgICAgICAgICAgICAgICA8RHJvcGRvd24uSXRlbT57dGhpcy5yZW5kZXJTZXR0aW5ncygpfTwvRHJvcGRvd24uSXRlbT5cclxuICAgICAgICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2V0dGluZ3MtaXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSBcIi9sb2dvdXRcIjtcclxuICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJzZXR0aW5ncy1sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICA8c3ZnIHdpZHRoPVwiMThcIiBoZWlnaHQ9XCIxOFwiIHZpZXdCb3g9XCIwIDAgMTggMThcIiBmaWxsPVwibm9uZVwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIj5cclxuICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPVwiTTEzLjc2OTggNC45ODA0N0MxNC43MTM2IDUuOTI0NTYgMTUuMzU2MyA3LjEyNzMgMTUuNjE2NiA4LjQzNjYxQzE1Ljg3NjggOS43NDU5MiAxNS43NDMgMTEuMTAzIDE1LjIzMjEgMTIuMzM2M0MxNC43MjExIDEzLjU2OTYgMTMuODU1OSAxNC42MjM2IDEyLjc0NiAxNS4zNjUyQzExLjYzNiAxNi4xMDY4IDEwLjMzMSAxNi41MDI3IDguOTk2MSAxNi41MDI3QzcuNjYxMTcgMTYuNTAyNyA2LjM1NjIxIDE2LjEwNjggNS4yNDYyMyAxNS4zNjUyQzQuMTM2MjQgMTQuNjIzNiAzLjI3MTA4IDEzLjU2OTYgMi43NjAxMiAxMi4zMzYzQzIuMjQ5MTYgMTEuMTAzIDIuMTE1MzYgOS43NDU5MiAyLjM3NTYzIDguNDM2NjFDMi42MzU5MSA3LjEyNzMgMy4yNzg1NiA1LjkyNDU2IDQuMjIyMzUgNC45ODA0N1wiIHN0cm9rZT1cImJsYWNrXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9XCJNOSAxLjVWOVwiIHN0cm9rZT1cImJsYWNrXCIgc3Ryb2tlLXdpZHRoPVwiMS41XCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPlxyXG4gICAgICAgICAgICAgICAgICA8L3N2Zz5cclxuICAgICAgICAgICAgICAgICAgICAgICBMb2dvdXQ8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAge3RoaXMucmVuZGVyT3JnTGlzdCgpfVxyXG4gICAgICAgICAgICAgIDwvRHJvcGRvd24uTWVudT5cclxuICAgICAgICAgICAgPC9Ecm9wZG93bj5cclxuICAgICAgICAgICAgey8qIDxOYXZEcm9wZG93blxyXG4gICAgICAgICAgICAgICAvLyB0aXRsZT17fVxyXG4gICAgICAgICAgICAgICAgYXM9ezxpIGNsYXNzTmFtZT1cImZhcyBmYS1jaGV2cm9uLWRvd25cIj48L2k+fVxyXG4gICAgICAgICAgICAgICAgaWQ9XCJjb2xsYXNpYmxlLW5hdi1kcm9wZG93blwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPE5hdkRyb3Bkb3duLkl0ZW0gaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICAgICAge3RoaXMucHJvcHMucHJvZmlsZS5lbWFpbCB8fCBcIlwifVxyXG4gICAgICAgICAgICAgICAgPC9OYXZEcm9wZG93bi5JdGVtPlxyXG4gICAgICAgICAgICAgICAgPE5hdkRyb3Bkb3duLkl0ZW0gaHJlZj1cIi9sb2dvdXRcIj5TaWduIE91dDwvTmF2RHJvcGRvd24uSXRlbT5cclxuICAgICAgICAgICAgICA8L05hdkRyb3Bkb3duPiAqL31cclxuICAgICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICAvLyA8L05hdmJhci5Db2xsYXBzZT5cclxuICAgICAgICApfVxyXG4gICAgICA8L05hdj5cclxuICAgICk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbnVsbCkoVXNlclByb2ZpbGUpO1xyXG4iXX0=