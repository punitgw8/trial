var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from "react";
import { Navbar, Button, Dropdown } from "react-bootstrap";
//import "./main.scss";
import UserProfile from "./userProfile";
import { getConsumedServices } from "../Services/utilities";
import queryString from "query-string";
//import BillingNavBar from "../billing/billingNavbar/billingNavbar";
import arrow from '../assets/images/icons/light-arrow-black.svg';
import arrowLeft from '../assets/images/icons/left-arrow-white.svg';
import _ from 'lodash';

var FEEDIO_UI_URL = process.env.REACT_APP_FEEDIO_UI_BASE_URL;
var CONTENTBASE_UI_URL = process.env.REACT_APP_CONTENTBASE_UI_BASE_URL;
var HITMAN_URL = process.env.REACT_APP_HITMAN_UI_BASE_URL;
var HTTPDUMP_URL = process.env.REACT_APP_HTTPDUMP_UI_BASE_URL;

var NavBar = function (_Component) {
  _inherits(NavBar, _Component);

  function NavBar() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, NavBar);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = NavBar.__proto__ || Object.getPrototypeOf(NavBar)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      firstName: "",
      lastName: "",
      email: ""
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(NavBar, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "navigateToProjectList",
    value: function navigateToProjectList() {
      this.props.history.push({
        pathname: "/orgs/" + this.props.organizationId + "/projects"
      });
    }
  }, {
    key: "navigateToHome",
    value: function navigateToHome() {
      if (!_.isEmpty(this.props.organizations)) {
        var orgId = this.props.organizationId;
        var pathname = "/orgs/" + orgId + "/ebl";
        var consumedServices = getConsumedServices(this.props.organizations, orgId);
        if (consumedServices.includes("ebl")) {
          pathname = "/orgs/" + orgId + "/projects";
        }
        this.props.history.push({
          pathname: pathname
        });
      }
    }
  }, {
    key: "navigateToDashboard",
    value: function navigateToDashboard() {
      var projectId = this.props.match.params.id;
      this.props.history.push({
        pathname: "/orgs/" + this.props.organizationId + "/projects/" + projectId
      });
    }
  }, {
    key: "navigateToProductDashboard",
    value: function navigateToProductDashboard() {
      var projectId = this.props.match.params.id;
      this.props.history.push({
        pathname: "/orgs/" + this.props.organizationId + "/products/"
      });
    }
  }, {
    key: "renderSearchDropdown",
    value: function renderSearchDropdown() {
      // const pathname = this.props.location.pathname;
      // return (
      //   pathname.includes("/projects") &&
      //   <GlobalSearch organizationId={this.props.organizationId} />
      // );
      return "hello";
    }
  }, {
    key: "renderNavbarBrand",
    value: function renderNavbarBrand() {
      var _this2 = this;

      var pathname = this.props.location.pathname;
      var socketIcon = process.env.REACT_APP_SOCKET_ICON;
      var socketLogo = process.env.REACT_APP_SOCKET_LOGO;
      return React.createElement(
        React.Fragment,
        null,
        React.createElement(
          Navbar.Brand,
          {
            className: "socket-navbar"
          },
          React.createElement(
            "div",
            { className: "navbar-logo" },
            React.createElement("img", {
              style: { cursor: "pointer" },
              src: pathname.includes("/products") ? socketLogo : socketIcon,
              alt: "viasocket-icon",
              width: "22px",
              height: "auto",
              onClick: function onClick() {
                return _this2.navigateToHome();
              }
            })
          )
        )
      );
    }
  }, {
    key: "renderBillingNavbar",
    value: function renderBillingNavbar() {
      // return(
      //   this.props.billingsNavTabs && (
      //     <Navbar
      //       id="function-nav"
      //       collapseOnSelect
      //       expand="lg"
      //       variant="light"
      //       className="px-0 socket-navbar"
      //     >
      //       <React.Fragment>
      //         <Navbar.Collapse className="justify-content-center">
      //           <BillingNavBar {...this.props}/>
      //         </Navbar.Collapse>
      //         <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      //       </React.Fragment>
      //     </Navbar>
      //   )
      // )
      return "Hello Billing";
    }
  }, {
    key: "renderNavigationTabs",
    value: function renderNavigationTabs() {
      // return (
      //   <>
      //     {this.props.showFunctionTabs && (
      //       <Navbar
      //         id="function-nav"
      //         collapseOnSelect
      //         expand="lg"
      //         variant="light"
      //         className="px-0 socket-navbar"
      //       >
      //         <React.Fragment>
      //           <Navbar.Collapse className="justify-content-center">
      //             <NavbarTabs {...this.props}></NavbarTabs>
      //           </Navbar.Collapse>
      //           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      //         </React.Fragment>
      //       </Navbar>
      //     )}
      //     {this.props.showOrgTabs && (
      //       <Navbar
      //         id="function-nav"
      //         collapseOnSelect
      //         expand="lg"
      //         variant="light"
      //         className="px-0 socket-navbar"
      //       >
      //         <React.Fragment>
      //           <Navbar.Collapse className="justify-content-center">
      //             <SettingsNavbarTabs {...this.props}></SettingsNavbarTabs>
      //           </Navbar.Collapse>
      //           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      //         </React.Fragment>
      //       </Navbar>
      //     )}
      //     {this.renderBillingNavbar()}
      //   </>
      // );
      return "Middle";
    }
  }, {
    key: "renderBreadcrumb",
    value: function renderBreadcrumb() {
      // return (
      //   <Breadcrumb {...this.props} />
      // )
      return "Breadcurmb";
    }
  }, {
    key: "switchProduct",
    value: function switchProduct(product) {
      var orgId = this.props.organizationId;
      var link = void 0;
      if (product === 'feedio') {
        link = FEEDIO_UI_URL + "/orgs/" + orgId;
      } else if (product === 'hitman') {
        link = HITMAN_URL + "/orgs/" + orgId;
      } else if (product === 'contentbase') {
        link = CONTENTBASE_UI_URL + "/orgs/" + orgId + "/projects";
      } else if (product === 'httpdump') {
        link = "" + HTTPDUMP_URL;
      }
      window.open(link, '_blank');
    }
  }, {
    key: "renderNewBreadcrumb",
    value: function renderNewBreadcrumb() {
      var _this3 = this;

      var projectId = this.props.match.params.id;
      var projectName = this.props.projects[projectId].name;
      return React.createElement(
        "div",
        { className: "custom-breadcrumb d-flex" },
        React.createElement(
          "div",
          { className: "link", onClick: function onClick() {
              _this3.navigateToHome();
            } },
          " EBL "
        ),
        projectName && React.createElement(
          "div",
          { className: "mx-1" },
          " ",
          '>',
          " "
        ),
        projectName && React.createElement(
          "div",
          { className: "link", onClick: function onClick() {
              _this3.navigateToHome();
            } },
          projectName
        )
      );
    }
  }, {
    key: "renderProjectName",
    value: function renderProjectName() {
      var _this4 = this;

      var projectId = this.props.match.params.id;
      var projectName = this.props.projects[projectId].name;
      return projectId && React.createElement(
        "div",
        { className: "link-new d-flex align-items-center", onClick: function onClick() {
            _this4.navigateToHome();
          } },
        React.createElement("img", { src: arrowLeft, className: "rotate-90 mr-10" }),
        projectName
      );
    }
  }, {
    key: "renderManageNavbarHeading",
    value: function renderManageNavbarHeading() {
      var pathArray = this.props.location.pathname.split('/');
      return this.props.manageHeading && pathArray[3] === "manage" && React.createElement(
        "div",
        { className: "tabs-wrapper manage-header-text" },
        pathArray[4] === "authkeys" ? "AuthKeys" : "Manage Team"
      );
    }
  }, {
    key: "renderBackOption",
    value: function renderBackOption() {
      var _this5 = this;

      var pathArray = this.props.location.pathname.split('/');
      return (pathArray[3] === "manage" || pathArray[3] === "billing") && React.createElement(
        "div",
        { className: "link-new d-flex align-items-center", onClick: function onClick() {
            return _this5.props.history.goBack();
          } },
        React.createElement("img", { src: arrowLeft, className: "rotate-90 mr-10" }),
        " Back"
      );
    }
  }, {
    key: "render",
    value: function render() {
      var _this6 = this;

      var pathname = this.props.location.pathname;
      this.product = queryString.parse(this.props.location.search).product;
      var isProjectDashboard = pathname.includes("/products");
      return React.createElement(
        React.Fragment,
        null,
        React.createElement(
          "div",
          { className: ['headerNavbar', isProjectDashboard ? 'headerNavbarWhite' : ''].join(" ") },
          React.createElement(
            "div",
            { className: "d-flex justify-content-between" },
            React.createElement(
              "div",
              { className: "logoContainer d-flex align-items-center" },
              React.createElement(
                "div",
                { className: "brand-block justify-content-left align-items-center" },
                this.props.showDashboardButton && React.createElement(
                  "div",
                  { className: "show-dashboard text-white d-flex justify-content-center ", onClick: function onClick() {
                      _this6.navigateToProductDashboard();
                    } },
                  React.createElement("img", { height: '16px', width: '16px', src: arrow, alt: "", "class": "mr-10 rotate-270" }),
                  "Go to Product Dashboard"
                ),
                !isProjectDashboard && React.createElement(
                  "div",
                  { className: "switchPrd m-1r" },
                  !this.props.showDashboardButton && React.createElement(
                    Dropdown,
                    null,
                    React.createElement(
                      Dropdown.Toggle,
                      { variant: "success", id: "dropdown-basic", className: "d-flex align-items-center" },
                      !this.product && this.renderNavbarBrand(),
                      " EBL ",
                      React.createElement(
                        "svg",
                        { className: "transition ml-1", width: "10", height: "6", viewBox: "0 0 10 6", fill: "none", xmlns: "http://www.w3.org/2000/svg" },
                        React.createElement("path", { d: "M1 1L5 5L9 1", stroke: "#EEEEEE", "stroke-width": "2", "stroke-linecap": "round", "stroke-linejoin": "round" })
                      )
                    ),
                    React.createElement(
                      Dropdown.Menu,
                      null,
                      React.createElement(
                        Dropdown.Item,
                        { href: "#", className: "dropHeader" },
                        "Switch to"
                      ),
                      React.createElement(
                        Dropdown.Item,
                        { href: "#", onClick: function onClick() {
                            _this6.switchProduct('feedio');
                          } },
                        "Feedio"
                      ),
                      React.createElement(
                        Dropdown.Item,
                        { href: "#", onClick: function onClick() {
                            _this6.switchProduct('hitman');
                          } },
                        "Hitman"
                      ),
                      React.createElement(
                        Dropdown.Item,
                        { href: "", onClick: function onClick() {
                            _this6.switchProduct('contentbase');
                          } },
                        "ContentBase"
                      ),
                      React.createElement(
                        Dropdown.Item,
                        { href: "", onClick: function onClick() {
                            _this6.switchProduct('httpdump');
                          } },
                        "Httpdump"
                      ),
                      React.createElement(
                        Dropdown.Item,
                        { href: "", onClick: function onClick() {
                            _this6.navigateToProductDashboard();
                          }, className: "go-to-link" },
                        React.createElement("img", { height: '16px', width: '16px', src: arrow, alt: "", "class": "mr-10 rotate-270" }),
                        "Go to Product dashboard"
                      )
                    )
                  )
                )
              ),
              !this.product && this.props.showBreadCrumb && this.renderProjectName(),
              this.renderBackOption()
            ),
            React.createElement(
              "div",
              { className: "d-flex" },
              this.renderManageNavbarHeading()
            ),
            React.createElement(
              "div",
              { className: "d-flex justify-content-end logoContainer" },
              this.renderSearchDropdown(),
              React.createElement(UserProfile, this.props)
            )
          )
        )
      );
    }
  }]);

  return NavBar;
}(Component);

export default NavBar;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9Db21wb25lbnRzL2hlYWRlci5qc3giXSwibmFtZXMiOlsiUmVhY3QiLCJDb21wb25lbnQiLCJOYXZiYXIiLCJCdXR0b24iLCJEcm9wZG93biIsIlVzZXJQcm9maWxlIiwiZ2V0Q29uc3VtZWRTZXJ2aWNlcyIsInF1ZXJ5U3RyaW5nIiwiYXJyb3ciLCJhcnJvd0xlZnQiLCJfIiwiRkVFRElPX1VJX1VSTCIsInByb2Nlc3MiLCJlbnYiLCJSRUFDVF9BUFBfRkVFRElPX1VJX0JBU0VfVVJMIiwiQ09OVEVOVEJBU0VfVUlfVVJMIiwiUkVBQ1RfQVBQX0NPTlRFTlRCQVNFX1VJX0JBU0VfVVJMIiwiSElUTUFOX1VSTCIsIlJFQUNUX0FQUF9ISVRNQU5fVUlfQkFTRV9VUkwiLCJIVFRQRFVNUF9VUkwiLCJSRUFDVF9BUFBfSFRUUERVTVBfVUlfQkFTRV9VUkwiLCJOYXZCYXIiLCJzdGF0ZSIsImZpcnN0TmFtZSIsImxhc3ROYW1lIiwiZW1haWwiLCJwcm9wcyIsImhpc3RvcnkiLCJwdXNoIiwicGF0aG5hbWUiLCJvcmdhbml6YXRpb25JZCIsImlzRW1wdHkiLCJvcmdhbml6YXRpb25zIiwib3JnSWQiLCJjb25zdW1lZFNlcnZpY2VzIiwiaW5jbHVkZXMiLCJwcm9qZWN0SWQiLCJtYXRjaCIsInBhcmFtcyIsImlkIiwibG9jYXRpb24iLCJzb2NrZXRJY29uIiwiUkVBQ1RfQVBQX1NPQ0tFVF9JQ09OIiwic29ja2V0TG9nbyIsIlJFQUNUX0FQUF9TT0NLRVRfTE9HTyIsImN1cnNvciIsIm5hdmlnYXRlVG9Ib21lIiwicHJvZHVjdCIsImxpbmsiLCJ3aW5kb3ciLCJvcGVuIiwicHJvamVjdE5hbWUiLCJwcm9qZWN0cyIsIm5hbWUiLCJwYXRoQXJyYXkiLCJzcGxpdCIsIm1hbmFnZUhlYWRpbmciLCJnb0JhY2siLCJwYXJzZSIsInNlYXJjaCIsImlzUHJvamVjdERhc2hib2FyZCIsImpvaW4iLCJzaG93RGFzaGJvYXJkQnV0dG9uIiwibmF2aWdhdGVUb1Byb2R1Y3REYXNoYm9hcmQiLCJyZW5kZXJOYXZiYXJCcmFuZCIsInN3aXRjaFByb2R1Y3QiLCJzaG93QnJlYWRDcnVtYiIsInJlbmRlclByb2plY3ROYW1lIiwicmVuZGVyQmFja09wdGlvbiIsInJlbmRlck1hbmFnZU5hdmJhckhlYWRpbmciLCJyZW5kZXJTZWFyY2hEcm9wZG93biJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPQSxLQUFQLElBQWdCQyxTQUFoQixRQUFpQyxPQUFqQztBQUNBLFNBQVNDLE1BQVQsRUFBaUJDLE1BQWpCLEVBQXlCQyxRQUF6QixRQUF5QyxpQkFBekM7QUFDQTtBQUNBLE9BQU9DLFdBQVAsTUFBd0IsZUFBeEI7QUFDQSxTQUFTQyxtQkFBVCxRQUFvQyx1QkFBcEM7QUFDQSxPQUFPQyxXQUFQLE1BQXdCLGNBQXhCO0FBQ0E7QUFDQSxPQUFPQyxLQUFQLE1BQWtCLDhDQUFsQjtBQUNBLE9BQU9DLFNBQVAsTUFBc0IsNkNBQXRCO0FBQ0EsT0FBT0MsQ0FBUCxNQUFjLFFBQWQ7O0FBRUEsSUFBTUMsZ0JBQWdCQyxRQUFRQyxHQUFSLENBQVlDLDRCQUFsQztBQUNBLElBQU1DLHFCQUFxQkgsUUFBUUMsR0FBUixDQUFZRyxpQ0FBdkM7QUFDQSxJQUFNQyxhQUFhTCxRQUFRQyxHQUFSLENBQVlLLDRCQUEvQjtBQUNBLElBQU1DLGVBQWVQLFFBQVFDLEdBQVIsQ0FBWU8sOEJBQWpDOztJQUVNQyxNOzs7Ozs7Ozs7Ozs7OztzTEFDSkMsSyxHQUFRO0FBQ05DLGlCQUFXLEVBREw7QUFFTkMsZ0JBQVUsRUFGSjtBQUdOQyxhQUFPO0FBSEQsSzs7Ozs7d0NBTVksQ0FBRTs7OzRDQUVFO0FBQ3RCLFdBQUtDLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQkMsSUFBbkIsQ0FBd0I7QUFDdEJDLDZCQUFtQixLQUFLSCxLQUFMLENBQVdJLGNBQTlCO0FBRHNCLE9BQXhCO0FBR0Q7OztxQ0FFZ0I7QUFDZixVQUFHLENBQUNwQixFQUFFcUIsT0FBRixDQUFVLEtBQUtMLEtBQUwsQ0FBV00sYUFBckIsQ0FBSixFQUF3QztBQUN0QyxZQUFNQyxRQUFRLEtBQUtQLEtBQUwsQ0FBV0ksY0FBekI7QUFDQSxZQUFJRCxzQkFBb0JJLEtBQXBCLFNBQUo7QUFDQSxZQUFJQyxtQkFBbUI1QixvQkFBb0IsS0FBS29CLEtBQUwsQ0FBV00sYUFBL0IsRUFBNkNDLEtBQTdDLENBQXZCO0FBQ0EsWUFBSUMsaUJBQWlCQyxRQUFqQixDQUEwQixLQUExQixDQUFKLEVBQXNDO0FBQ3BDTixnQ0FBb0JJLEtBQXBCO0FBQ0Q7QUFDRCxhQUFLUCxLQUFMLENBQVdDLE9BQVgsQ0FBbUJDLElBQW5CLENBQXdCO0FBQ3RCQztBQURzQixTQUF4QjtBQUdEO0FBQ0Y7OzswQ0FFcUI7QUFDcEIsVUFBSU8sWUFBWSxLQUFLVixLQUFMLENBQVdXLEtBQVgsQ0FBaUJDLE1BQWpCLENBQXdCQyxFQUF4QztBQUNBLFdBQUtiLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQkMsSUFBbkIsQ0FBd0I7QUFDdEJDLDZCQUFtQixLQUFLSCxLQUFMLENBQVdJLGNBQTlCLGtCQUF5RE07QUFEbkMsT0FBeEI7QUFHRDs7O2lEQUU0QjtBQUMzQixVQUFJQSxZQUFZLEtBQUtWLEtBQUwsQ0FBV1csS0FBWCxDQUFpQkMsTUFBakIsQ0FBd0JDLEVBQXhDO0FBQ0EsV0FBS2IsS0FBTCxDQUFXQyxPQUFYLENBQW1CQyxJQUFuQixDQUF3QjtBQUN0QkMsNkJBQW1CLEtBQUtILEtBQUwsQ0FBV0ksY0FBOUI7QUFEc0IsT0FBeEI7QUFHRDs7OzJDQUVzQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBTyxPQUFQO0FBQ0Q7Ozt3Q0FFbUI7QUFBQTs7QUFDbEIsVUFBTUQsV0FBVyxLQUFLSCxLQUFMLENBQVdjLFFBQVgsQ0FBb0JYLFFBQXJDO0FBQ0EsVUFBTVksYUFBYTdCLFFBQVFDLEdBQVIsQ0FBWTZCLHFCQUEvQjtBQUNBLFVBQU1DLGFBQWEvQixRQUFRQyxHQUFSLENBQVkrQixxQkFBL0I7QUFDQSxhQUNHO0FBQUMsYUFBRCxDQUFPLFFBQVA7QUFBQTtBQUNDO0FBQUMsZ0JBQUQsQ0FBUSxLQUFSO0FBQUE7QUFDRSx1QkFBVTtBQURaO0FBR0U7QUFBQTtBQUFBLGNBQUssV0FBVSxhQUFmO0FBQ0U7QUFDRSxxQkFBTyxFQUFFQyxRQUFRLFNBQVYsRUFEVDtBQUVFLG1CQUFNaEIsU0FBU00sUUFBVCxDQUFrQixXQUFsQixJQUFpQ1EsVUFBakMsR0FBOENGLFVBRnREO0FBR0UsbUJBQUksZ0JBSE47QUFJRSxxQkFBTSxNQUpSO0FBS0Usc0JBQU8sTUFMVDtBQU1FLHVCQUFTO0FBQUEsdUJBQU0sT0FBS0ssY0FBTCxFQUFOO0FBQUE7QUFOWDtBQURGO0FBSEY7QUFERCxPQURIO0FBa0JEOzs7MENBRW9CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQU8sZUFBUDtBQUNEOzs7MkNBRXNCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBTyxRQUFQO0FBQ0Q7Ozt1Q0FFa0I7QUFDakI7QUFDQTtBQUNBO0FBQ0EsYUFBTyxZQUFQO0FBQ0Q7OztrQ0FHYUMsTyxFQUFRO0FBQ3BCLFVBQUlkLFFBQVEsS0FBS1AsS0FBTCxDQUFXSSxjQUF2QjtBQUNBLFVBQUlrQixhQUFKO0FBQ0EsVUFBR0QsWUFBVSxRQUFiLEVBQXNCO0FBQ3JCQyxlQUFRckMsYUFBUixjQUE4QnNCLEtBQTlCO0FBQ0EsT0FGRCxNQUdLLElBQUdjLFlBQVUsUUFBYixFQUFzQjtBQUN6QkMsZUFBVS9CLFVBQVYsY0FBNkJnQixLQUE3QjtBQUNELE9BRkksTUFHQSxJQUFHYyxZQUFVLGFBQWIsRUFBMkI7QUFDOUJDLGVBQVVqQyxrQkFBVixjQUFxQ2tCLEtBQXJDO0FBQ0QsT0FGSSxNQUdBLElBQUdjLFlBQVUsVUFBYixFQUF3QjtBQUMzQkMsb0JBQVU3QixZQUFWO0FBQ0Q7QUFDRDhCLGFBQU9DLElBQVAsQ0FBWUYsSUFBWixFQUFpQixRQUFqQjtBQUNEOzs7MENBRXFCO0FBQUE7O0FBQ3BCLFVBQUlaLFlBQVksS0FBS1YsS0FBTCxDQUFXVyxLQUFYLENBQWlCQyxNQUFqQixDQUF3QkMsRUFBeEM7QUFDQSxVQUFJWSxjQUFjLEtBQUt6QixLQUFMLENBQVcwQixRQUFYLENBQW9CaEIsU0FBcEIsRUFBK0JpQixJQUFqRDtBQUNBLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBVSwwQkFBZjtBQUNFO0FBQUE7QUFBQSxZQUFLLFdBQVUsTUFBZixFQUFzQixTQUFTLG1CQUFNO0FBQUUscUJBQUtQLGNBQUw7QUFBc0IsYUFBN0Q7QUFBQTtBQUFBLFNBREY7QUFFR0ssdUJBQWU7QUFBQTtBQUFBLFlBQUssV0FBVSxNQUFmO0FBQUE7QUFBeUIsYUFBekI7QUFBQTtBQUFBLFNBRmxCO0FBR0dBLHVCQUFlO0FBQUE7QUFBQSxZQUFLLFdBQVUsTUFBZixFQUFzQixTQUFTLG1CQUFNO0FBQUUscUJBQUtMLGNBQUw7QUFBc0IsYUFBN0Q7QUFBZ0VLO0FBQWhFO0FBSGxCLE9BREY7QUFPRDs7O3dDQUNtQjtBQUFBOztBQUNsQixVQUFJZixZQUFZLEtBQUtWLEtBQUwsQ0FBV1csS0FBWCxDQUFpQkMsTUFBakIsQ0FBd0JDLEVBQXhDO0FBQ0EsVUFBSVksY0FBYyxLQUFLekIsS0FBTCxDQUFXMEIsUUFBWCxDQUFvQmhCLFNBQXBCLEVBQStCaUIsSUFBakQ7QUFDQSxhQUNFakIsYUFBYTtBQUFBO0FBQUEsVUFBTSxXQUFVLG9DQUFoQixFQUFxRCxTQUFTLG1CQUFNO0FBQUUsbUJBQUtVLGNBQUw7QUFBc0IsV0FBNUY7QUFDWCxxQ0FBSyxLQUFLckMsU0FBVixFQUFxQixXQUFVLGlCQUEvQixHQURXO0FBRVQwQztBQUZTLE9BRGY7QUFNRDs7O2dEQUUwQjtBQUN6QixVQUFJRyxZQUFZLEtBQUs1QixLQUFMLENBQVdjLFFBQVgsQ0FBb0JYLFFBQXBCLENBQTZCMEIsS0FBN0IsQ0FBbUMsR0FBbkMsQ0FBaEI7QUFDQSxhQUNFLEtBQUs3QixLQUFMLENBQVc4QixhQUFYLElBQTRCRixVQUFVLENBQVYsTUFBZSxRQUEzQyxJQUF1RDtBQUFBO0FBQUEsVUFBSyxXQUFVLGlDQUFmO0FBQ3BEQSxrQkFBVSxDQUFWLE1BQWUsVUFBZixHQUE0QixVQUE1QixHQUF5QztBQURXLE9BRHpEO0FBS0Q7Ozt1Q0FFaUI7QUFBQTs7QUFDaEIsVUFBSUEsWUFBWSxLQUFLNUIsS0FBTCxDQUFXYyxRQUFYLENBQW9CWCxRQUFwQixDQUE2QjBCLEtBQTdCLENBQW1DLEdBQW5DLENBQWhCO0FBQ0EsYUFDRSxDQUFDRCxVQUFVLENBQVYsTUFBZSxRQUFmLElBQTBCQSxVQUFVLENBQVYsTUFBZSxTQUExQyxLQUF3RDtBQUFBO0FBQUEsVUFBSyxXQUFVLG9DQUFmLEVBQW9ELFNBQVM7QUFBQSxtQkFBTSxPQUFLNUIsS0FBTCxDQUFXQyxPQUFYLENBQW1COEIsTUFBbkIsRUFBTjtBQUFBLFdBQTdEO0FBQ3RELHFDQUFLLEtBQUtoRCxTQUFWLEVBQXFCLFdBQVUsaUJBQS9CLEdBRHNEO0FBRXJEO0FBRnFELE9BRDFEO0FBTUQ7Ozs2QkFFUTtBQUFBOztBQUNQLFVBQU1vQixXQUFXLEtBQUtILEtBQUwsQ0FBV2MsUUFBWCxDQUFvQlgsUUFBckM7QUFDQSxXQUFLa0IsT0FBTCxHQUFleEMsWUFBWW1ELEtBQVosQ0FBa0IsS0FBS2hDLEtBQUwsQ0FBV2MsUUFBWCxDQUFvQm1CLE1BQXRDLEVBQThDWixPQUE3RDtBQUNBLFVBQUlhLHFCQUFxQi9CLFNBQVNNLFFBQVQsQ0FBa0IsV0FBbEIsQ0FBekI7QUFDQSxhQUNFO0FBQUMsYUFBRCxDQUFPLFFBQVA7QUFBQTtBQUNFO0FBQUE7QUFBQSxZQUFLLFdBQVcsQ0FBQyxjQUFELEVBQWdCeUIscUJBQW9CLG1CQUFwQixHQUEwQyxFQUExRCxFQUE4REMsSUFBOUQsQ0FBbUUsR0FBbkUsQ0FBaEI7QUFDRTtBQUFBO0FBQUEsY0FBSyxXQUFVLGdDQUFmO0FBQ0U7QUFBQTtBQUFBLGdCQUFLLFdBQVUseUNBQWY7QUFDRTtBQUFBO0FBQUEsa0JBQUssV0FBVSxxREFBZjtBQUNDLHFCQUFLbkMsS0FBTCxDQUFXb0MsbUJBQVgsSUFBZ0M7QUFBQTtBQUFBLG9CQUFLLFdBQVUsMERBQWYsRUFBMEUsU0FBUyxtQkFBTTtBQUFFLDZCQUFLQywwQkFBTDtBQUFrQyxxQkFBN0g7QUFBK0gsK0NBQUssUUFBUSxNQUFiLEVBQXFCLE9BQU8sTUFBNUIsRUFBb0MsS0FBS3ZELEtBQXpDLEVBQWdELEtBQUksRUFBcEQsRUFBdUQsU0FBTSxrQkFBN0QsR0FBL0g7QUFBQTtBQUFBLGlCQURqQztBQUlBLGlCQUFDb0Qsa0JBQUQsSUFBdUI7QUFBQTtBQUFBLG9CQUFLLFdBQVUsZ0JBQWY7QUFDWixtQkFBQyxLQUFLbEMsS0FBTCxDQUFXb0MsbUJBQVosSUFBaUM7QUFBQyw0QkFBRDtBQUFBO0FBQ2hDO0FBQUMsOEJBQUQsQ0FBVSxNQUFWO0FBQUEsd0JBQWlCLFNBQVEsU0FBekIsRUFBbUMsSUFBRyxnQkFBdEMsRUFBdUQsV0FBVSwyQkFBakU7QUFDQyx1QkFBQyxLQUFLZixPQUFOLElBQWlCLEtBQUtpQixpQkFBTCxFQURsQjtBQUFBO0FBQ2dEO0FBQUE7QUFBQSwwQkFBSyxXQUFVLGlCQUFmLEVBQWlDLE9BQU0sSUFBdkMsRUFBNEMsUUFBTyxHQUFuRCxFQUF1RCxTQUFRLFVBQS9ELEVBQTBFLE1BQUssTUFBL0UsRUFBc0YsT0FBTSw0QkFBNUY7QUFBeUgsc0RBQU0sR0FBRSxjQUFSLEVBQXVCLFFBQU8sU0FBOUIsRUFBd0MsZ0JBQWEsR0FBckQsRUFBeUQsa0JBQWUsT0FBeEUsRUFBZ0YsbUJBQWdCLE9BQWhHO0FBQXpIO0FBRGhELHFCQURnQztBQUtoQztBQUFDLDhCQUFELENBQVUsSUFBVjtBQUFBO0FBQ0U7QUFBQyxnQ0FBRCxDQUFVLElBQVY7QUFBQSwwQkFBZSxNQUFLLEdBQXBCLEVBQXdCLFdBQVUsWUFBbEM7QUFBQTtBQUFBLHVCQURGO0FBSUU7QUFBQyxnQ0FBRCxDQUFVLElBQVY7QUFBQSwwQkFBZSxNQUFLLEdBQXBCLEVBQXdCLFNBQVMsbUJBQU07QUFBRSxtQ0FBS0MsYUFBTCxDQUFtQixRQUFuQjtBQUE2QiwyQkFBdEU7QUFBQTtBQUFBLHVCQUpGO0FBT0U7QUFBQyxnQ0FBRCxDQUFVLElBQVY7QUFBQSwwQkFBZSxNQUFLLEdBQXBCLEVBQXdCLFNBQVMsbUJBQU07QUFBRSxtQ0FBS0EsYUFBTCxDQUFtQixRQUFuQjtBQUE4QiwyQkFBdkU7QUFBQTtBQUFBLHVCQVBGO0FBVUU7QUFBQyxnQ0FBRCxDQUFVLElBQVY7QUFBQSwwQkFBZSxNQUFLLEVBQXBCLEVBQXVCLFNBQVMsbUJBQU07QUFBRSxtQ0FBS0EsYUFBTCxDQUFtQixhQUFuQjtBQUFtQywyQkFBM0U7QUFBQTtBQUFBLHVCQVZGO0FBYUU7QUFBQyxnQ0FBRCxDQUFVLElBQVY7QUFBQSwwQkFBZSxNQUFLLEVBQXBCLEVBQXVCLFNBQVMsbUJBQU07QUFBRSxtQ0FBS0EsYUFBTCxDQUFtQixVQUFuQjtBQUFnQywyQkFBeEU7QUFBQTtBQUFBLHVCQWJGO0FBZ0JFO0FBQUMsZ0NBQUQsQ0FBVSxJQUFWO0FBQUEsMEJBQWUsTUFBSyxFQUFwQixFQUF1QixTQUFTLG1CQUFNO0FBQUUsbUNBQUtGLDBCQUFMO0FBQW1DLDJCQUEzRSxFQUE2RSxXQUFVLFlBQXZGO0FBQ0UscURBQUssUUFBUSxNQUFiLEVBQXFCLE9BQU8sTUFBNUIsRUFBb0MsS0FBS3ZELEtBQXpDLEVBQWdELEtBQUksRUFBcEQsRUFBdUQsU0FBTSxrQkFBN0QsR0FERjtBQUFBO0FBQUE7QUFoQkY7QUFMZ0M7QUFEckI7QUFKdkIsZUFERjtBQW9DRyxlQUFDLEtBQUt1QyxPQUFOLElBQWlCLEtBQUtyQixLQUFMLENBQVd3QyxjQUE1QixJQUE4QyxLQUFLQyxpQkFBTCxFQXBDakQ7QUFxQ0csbUJBQUtDLGdCQUFMO0FBckNILGFBREY7QUF3Q0U7QUFBQTtBQUFBLGdCQUFLLFdBQVUsUUFBZjtBQUtHLG1CQUFLQyx5QkFBTDtBQUxILGFBeENGO0FBK0NFO0FBQUE7QUFBQSxnQkFBSyxXQUFVLDBDQUFmO0FBRUcsbUJBQUtDLG9CQUFMLEVBRkg7QUFNRSxrQ0FBQyxXQUFELEVBQWlCLEtBQUs1QyxLQUF0QjtBQU5GO0FBL0NGO0FBREY7QUFERixPQURGO0FBK0REOzs7O0VBalJrQnpCLFM7O0FBb1JyQixlQUFlb0IsTUFBZiIsImZpbGUiOiJoZWFkZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IE5hdmJhciwgQnV0dG9uLCBEcm9wZG93biB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuLy9pbXBvcnQgXCIuL21haW4uc2Nzc1wiO1xyXG5pbXBvcnQgVXNlclByb2ZpbGUgZnJvbSBcIi4vdXNlclByb2ZpbGVcIjtcclxuaW1wb3J0IHsgZ2V0Q29uc3VtZWRTZXJ2aWNlcyB9IGZyb20gXCIuLi9TZXJ2aWNlcy91dGlsaXRpZXNcIjtcclxuaW1wb3J0IHF1ZXJ5U3RyaW5nIGZyb20gXCJxdWVyeS1zdHJpbmdcIjtcclxuLy9pbXBvcnQgQmlsbGluZ05hdkJhciBmcm9tIFwiLi4vYmlsbGluZy9iaWxsaW5nTmF2YmFyL2JpbGxpbmdOYXZiYXJcIjtcclxuaW1wb3J0IGFycm93IGZyb20gJy4uL2Fzc2V0cy9pbWFnZXMvaWNvbnMvbGlnaHQtYXJyb3ctYmxhY2suc3ZnJztcclxuaW1wb3J0IGFycm93TGVmdCBmcm9tICcuLi9hc3NldHMvaW1hZ2VzL2ljb25zL2xlZnQtYXJyb3ctd2hpdGUuc3ZnJztcclxuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbmNvbnN0IEZFRURJT19VSV9VUkwgPSBwcm9jZXNzLmVudi5SRUFDVF9BUFBfRkVFRElPX1VJX0JBU0VfVVJMXHJcbmNvbnN0IENPTlRFTlRCQVNFX1VJX1VSTCA9IHByb2Nlc3MuZW52LlJFQUNUX0FQUF9DT05URU5UQkFTRV9VSV9CQVNFX1VSTFxyXG5jb25zdCBISVRNQU5fVVJMID0gcHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0hJVE1BTl9VSV9CQVNFX1VSTFxyXG5jb25zdCBIVFRQRFVNUF9VUkwgPSBwcm9jZXNzLmVudi5SRUFDVF9BUFBfSFRUUERVTVBfVUlfQkFTRV9VUkxcclxuXHJcbmNsYXNzIE5hdkJhciBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgc3RhdGUgPSB7XHJcbiAgICBmaXJzdE5hbWU6IFwiXCIsXHJcbiAgICBsYXN0TmFtZTogXCJcIixcclxuICAgIGVtYWlsOiBcIlwiLFxyXG4gIH07XHJcblxyXG4gIGNvbXBvbmVudERpZE1vdW50KCkge31cclxuXHJcbiAgbmF2aWdhdGVUb1Byb2plY3RMaXN0KCkge1xyXG4gICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goe1xyXG4gICAgICBwYXRobmFtZTogYC9vcmdzLyR7dGhpcy5wcm9wcy5vcmdhbml6YXRpb25JZH0vcHJvamVjdHNgLFxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuYXZpZ2F0ZVRvSG9tZSgpIHtcclxuICAgIGlmKCFfLmlzRW1wdHkodGhpcy5wcm9wcy5vcmdhbml6YXRpb25zKSl7XHJcbiAgICAgIGNvbnN0IG9yZ0lkID0gdGhpcy5wcm9wcy5vcmdhbml6YXRpb25JZFxyXG4gICAgICBsZXQgcGF0aG5hbWUgPSBgL29yZ3MvJHtvcmdJZH0vZWJsYFxyXG4gICAgICBsZXQgY29uc3VtZWRTZXJ2aWNlcyA9IGdldENvbnN1bWVkU2VydmljZXModGhpcy5wcm9wcy5vcmdhbml6YXRpb25zLG9yZ0lkKVxyXG4gICAgICBpZiAoY29uc3VtZWRTZXJ2aWNlcy5pbmNsdWRlcyhcImVibFwiKSkge1xyXG4gICAgICAgIHBhdGhuYW1lID0gYC9vcmdzLyR7b3JnSWR9L3Byb2plY3RzYFxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKHtcclxuICAgICAgICBwYXRobmFtZSxcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuYXZpZ2F0ZVRvRGFzaGJvYXJkKCkge1xyXG4gICAgbGV0IHByb2plY3RJZCA9IHRoaXMucHJvcHMubWF0Y2gucGFyYW1zLmlkO1xyXG4gICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goe1xyXG4gICAgICBwYXRobmFtZTogYC9vcmdzLyR7dGhpcy5wcm9wcy5vcmdhbml6YXRpb25JZH0vcHJvamVjdHMvJHtwcm9qZWN0SWR9YCxcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmF2aWdhdGVUb1Byb2R1Y3REYXNoYm9hcmQoKSB7XHJcbiAgICBsZXQgcHJvamVjdElkID0gdGhpcy5wcm9wcy5tYXRjaC5wYXJhbXMuaWQ7XHJcbiAgICB0aGlzLnByb3BzLmhpc3RvcnkucHVzaCh7XHJcbiAgICAgIHBhdGhuYW1lOiBgL29yZ3MvJHt0aGlzLnByb3BzLm9yZ2FuaXphdGlvbklkfS9wcm9kdWN0cy9gLFxyXG4gICAgfSk7XHJcbiAgfSAgXHJcblxyXG4gIHJlbmRlclNlYXJjaERyb3Bkb3duKCkge1xyXG4gICAgLy8gY29uc3QgcGF0aG5hbWUgPSB0aGlzLnByb3BzLmxvY2F0aW9uLnBhdGhuYW1lO1xyXG4gICAgLy8gcmV0dXJuIChcclxuICAgIC8vICAgcGF0aG5hbWUuaW5jbHVkZXMoXCIvcHJvamVjdHNcIikgJiZcclxuICAgIC8vICAgPEdsb2JhbFNlYXJjaCBvcmdhbml6YXRpb25JZD17dGhpcy5wcm9wcy5vcmdhbml6YXRpb25JZH0gLz5cclxuICAgIC8vICk7XHJcbiAgICByZXR1cm4gXCJoZWxsb1wiO1xyXG4gIH1cclxuXHJcbiAgcmVuZGVyTmF2YmFyQnJhbmQoKSB7XHJcbiAgICBjb25zdCBwYXRobmFtZSA9IHRoaXMucHJvcHMubG9jYXRpb24ucGF0aG5hbWU7XHJcbiAgICBjb25zdCBzb2NrZXRJY29uID0gcHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX1NPQ0tFVF9JQ09OO1xyXG4gICAgY29uc3Qgc29ja2V0TG9nbyA9IHByb2Nlc3MuZW52LlJFQUNUX0FQUF9TT0NLRVRfTE9HTztcclxuICAgIHJldHVybiAoXHJcbiAgICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgPE5hdmJhci5CcmFuZFxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwic29ja2V0LW5hdmJhclwiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuYXZiYXItbG9nb1wiPlxyXG4gICAgICAgICAgICA8aW1nXHJcbiAgICAgICAgICAgICAgc3R5bGU9e3sgY3Vyc29yOiBcInBvaW50ZXJcIiB9fVxyXG4gICAgICAgICAgICAgIHNyYz17IHBhdGhuYW1lLmluY2x1ZGVzKFwiL3Byb2R1Y3RzXCIpID8gc29ja2V0TG9nbyA6IHNvY2tldEljb24gfVxyXG4gICAgICAgICAgICAgIGFsdD1cInZpYXNvY2tldC1pY29uXCJcclxuICAgICAgICAgICAgICB3aWR0aD1cIjIycHhcIlxyXG4gICAgICAgICAgICAgIGhlaWdodD1cImF1dG9cIlxyXG4gICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMubmF2aWdhdGVUb0hvbWUoKX1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvTmF2YmFyLkJyYW5kPlxyXG4gICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHJlbmRlckJpbGxpbmdOYXZiYXIoKXtcclxuICAgIC8vIHJldHVybihcclxuICAgIC8vICAgdGhpcy5wcm9wcy5iaWxsaW5nc05hdlRhYnMgJiYgKFxyXG4gICAgLy8gICAgIDxOYXZiYXJcclxuICAgIC8vICAgICAgIGlkPVwiZnVuY3Rpb24tbmF2XCJcclxuICAgIC8vICAgICAgIGNvbGxhcHNlT25TZWxlY3RcclxuICAgIC8vICAgICAgIGV4cGFuZD1cImxnXCJcclxuICAgIC8vICAgICAgIHZhcmlhbnQ9XCJsaWdodFwiXHJcbiAgICAvLyAgICAgICBjbGFzc05hbWU9XCJweC0wIHNvY2tldC1uYXZiYXJcIlxyXG4gICAgLy8gICAgID5cclxuICAgIC8vICAgICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgIC8vICAgICAgICAgPE5hdmJhci5Db2xsYXBzZSBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCI+XHJcbiAgICAvLyAgICAgICAgICAgPEJpbGxpbmdOYXZCYXIgey4uLnRoaXMucHJvcHN9Lz5cclxuICAgIC8vICAgICAgICAgPC9OYXZiYXIuQ29sbGFwc2U+XHJcbiAgICAvLyAgICAgICAgIDxOYXZiYXIuVG9nZ2xlIGFyaWEtY29udHJvbHM9XCJyZXNwb25zaXZlLW5hdmJhci1uYXZcIiAvPlxyXG4gICAgLy8gICAgICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICAgIC8vICAgICA8L05hdmJhcj5cclxuICAgIC8vICAgKVxyXG4gICAgLy8gKVxyXG4gICAgcmV0dXJuIFwiSGVsbG8gQmlsbGluZ1wiXHJcbiAgfVxyXG5cclxuICByZW5kZXJOYXZpZ2F0aW9uVGFicygpIHtcclxuICAgIC8vIHJldHVybiAoXHJcbiAgICAvLyAgIDw+XHJcbiAgICAvLyAgICAge3RoaXMucHJvcHMuc2hvd0Z1bmN0aW9uVGFicyAmJiAoXHJcbiAgICAvLyAgICAgICA8TmF2YmFyXHJcbiAgICAvLyAgICAgICAgIGlkPVwiZnVuY3Rpb24tbmF2XCJcclxuICAgIC8vICAgICAgICAgY29sbGFwc2VPblNlbGVjdFxyXG4gICAgLy8gICAgICAgICBleHBhbmQ9XCJsZ1wiXHJcbiAgICAvLyAgICAgICAgIHZhcmlhbnQ9XCJsaWdodFwiXHJcbiAgICAvLyAgICAgICAgIGNsYXNzTmFtZT1cInB4LTAgc29ja2V0LW5hdmJhclwiXHJcbiAgICAvLyAgICAgICA+XHJcbiAgICAvLyAgICAgICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgIC8vICAgICAgICAgICA8TmF2YmFyLkNvbGxhcHNlIGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1jZW50ZXJcIj5cclxuICAgIC8vICAgICAgICAgICAgIDxOYXZiYXJUYWJzIHsuLi50aGlzLnByb3BzfT48L05hdmJhclRhYnM+XHJcbiAgICAvLyAgICAgICAgICAgPC9OYXZiYXIuQ29sbGFwc2U+XHJcbiAgICAvLyAgICAgICAgICAgPE5hdmJhci5Ub2dnbGUgYXJpYS1jb250cm9scz1cInJlc3BvbnNpdmUtbmF2YmFyLW5hdlwiIC8+XHJcbiAgICAvLyAgICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAvLyAgICAgICA8L05hdmJhcj5cclxuICAgIC8vICAgICApfVxyXG4gICAgLy8gICAgIHt0aGlzLnByb3BzLnNob3dPcmdUYWJzICYmIChcclxuICAgIC8vICAgICAgIDxOYXZiYXJcclxuICAgIC8vICAgICAgICAgaWQ9XCJmdW5jdGlvbi1uYXZcIlxyXG4gICAgLy8gICAgICAgICBjb2xsYXBzZU9uU2VsZWN0XHJcbiAgICAvLyAgICAgICAgIGV4cGFuZD1cImxnXCJcclxuICAgIC8vICAgICAgICAgdmFyaWFudD1cImxpZ2h0XCJcclxuICAgIC8vICAgICAgICAgY2xhc3NOYW1lPVwicHgtMCBzb2NrZXQtbmF2YmFyXCJcclxuICAgIC8vICAgICAgID5cclxuICAgIC8vICAgICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgLy8gICAgICAgICAgIDxOYXZiYXIuQ29sbGFwc2UgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgLy8gICAgICAgICAgICAgPFNldHRpbmdzTmF2YmFyVGFicyB7Li4udGhpcy5wcm9wc30+PC9TZXR0aW5nc05hdmJhclRhYnM+XHJcbiAgICAvLyAgICAgICAgICAgPC9OYXZiYXIuQ29sbGFwc2U+XHJcbiAgICAvLyAgICAgICAgICAgPE5hdmJhci5Ub2dnbGUgYXJpYS1jb250cm9scz1cInJlc3BvbnNpdmUtbmF2YmFyLW5hdlwiIC8+XHJcbiAgICAvLyAgICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAvLyAgICAgICA8L05hdmJhcj5cclxuICAgIC8vICAgICApfVxyXG4gICAgLy8gICAgIHt0aGlzLnJlbmRlckJpbGxpbmdOYXZiYXIoKX1cclxuICAgIC8vICAgPC8+XHJcbiAgICAvLyApO1xyXG4gICAgcmV0dXJuIFwiTWlkZGxlXCI7XHJcbiAgfVxyXG5cclxuICByZW5kZXJCcmVhZGNydW1iKCkge1xyXG4gICAgLy8gcmV0dXJuIChcclxuICAgIC8vICAgPEJyZWFkY3J1bWIgey4uLnRoaXMucHJvcHN9IC8+XHJcbiAgICAvLyApXHJcbiAgICByZXR1cm4gXCJCcmVhZGN1cm1iXCI7XHJcbiAgfVxyXG5cclxuXHJcbiAgc3dpdGNoUHJvZHVjdChwcm9kdWN0KXtcclxuICAgIGxldCBvcmdJZCA9IHRoaXMucHJvcHMub3JnYW5pemF0aW9uSWQ7XHJcbiAgICBsZXQgbGluaztcclxuICAgIGlmKHByb2R1Y3Q9PT0nZmVlZGlvJyl7XHJcbiAgICAgbGluaz1gJHtGRUVESU9fVUlfVVJMfS9vcmdzLyR7b3JnSWR9YFxyXG4gICAgfVxyXG4gICAgZWxzZSBpZihwcm9kdWN0PT09J2hpdG1hbicpe1xyXG4gICAgICBsaW5rID0gYCR7SElUTUFOX1VSTH0vb3Jncy8ke29yZ0lkfWBcclxuICAgIH1cclxuICAgIGVsc2UgaWYocHJvZHVjdD09PSdjb250ZW50YmFzZScpe1xyXG4gICAgICBsaW5rID0gYCR7Q09OVEVOVEJBU0VfVUlfVVJMfS9vcmdzLyR7b3JnSWR9L3Byb2plY3RzYFxyXG4gICAgfVxyXG4gICAgZWxzZSBpZihwcm9kdWN0PT09J2h0dHBkdW1wJyl7XHJcbiAgICAgIGxpbmsgPSBgJHtIVFRQRFVNUF9VUkx9YFxyXG4gICAgfVxyXG4gICAgd2luZG93Lm9wZW4obGluaywnX2JsYW5rJyk7XHJcbiAgfVxyXG5cclxuICByZW5kZXJOZXdCcmVhZGNydW1iKCkge1xyXG4gICAgbGV0IHByb2plY3RJZCA9IHRoaXMucHJvcHMubWF0Y2gucGFyYW1zLmlkO1xyXG4gICAgbGV0IHByb2plY3ROYW1lID0gdGhpcy5wcm9wcy5wcm9qZWN0c1twcm9qZWN0SWRdLm5hbWU7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImN1c3RvbS1icmVhZGNydW1iIGQtZmxleFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGlua1wiIG9uQ2xpY2s9eygpID0+IHsgdGhpcy5uYXZpZ2F0ZVRvSG9tZSgpfSB9PiBFQkwgPC9kaXY+XHJcbiAgICAgICAge3Byb2plY3ROYW1lICYmIDxkaXYgY2xhc3NOYW1lPVwibXgtMVwiPiB7ICc+JyB9IDwvZGl2PiB9XHJcbiAgICAgICAge3Byb2plY3ROYW1lICYmIDxkaXYgY2xhc3NOYW1lPVwibGlua1wiIG9uQ2xpY2s9eygpID0+IHsgdGhpcy5uYXZpZ2F0ZVRvSG9tZSgpfX0+e3Byb2plY3ROYW1lfTwvZGl2Pn1cclxuICAgICAgPC9kaXY+XHJcbiAgICApXHJcbiAgfVxyXG4gIHJlbmRlclByb2plY3ROYW1lKCkge1xyXG4gICAgbGV0IHByb2plY3RJZCA9IHRoaXMucHJvcHMubWF0Y2gucGFyYW1zLmlkO1xyXG4gICAgbGV0IHByb2plY3ROYW1lID0gdGhpcy5wcm9wcy5wcm9qZWN0c1twcm9qZWN0SWRdLm5hbWU7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICBwcm9qZWN0SWQgJiYgPGRpdiAgY2xhc3NOYW1lPVwibGluay1uZXcgZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiIG9uQ2xpY2s9eygpID0+IHsgdGhpcy5uYXZpZ2F0ZVRvSG9tZSgpfSB9PiBcclxuICAgICAgICA8aW1nIHNyYz17YXJyb3dMZWZ0fSBjbGFzc05hbWU9XCJyb3RhdGUtOTAgbXItMTBcIiAvPlxyXG4gICAgICAgIHsgcHJvamVjdE5hbWV9XHJcbiAgICAgIDwvZGl2PiBcclxuICAgIClcclxuICB9XHJcblxyXG4gIHJlbmRlck1hbmFnZU5hdmJhckhlYWRpbmcoKXtcclxuICAgIGxldCBwYXRoQXJyYXkgPSB0aGlzLnByb3BzLmxvY2F0aW9uLnBhdGhuYW1lLnNwbGl0KCcvJyk7XHJcbiAgICByZXR1cm4oXHJcbiAgICAgIHRoaXMucHJvcHMubWFuYWdlSGVhZGluZyAmJiBwYXRoQXJyYXlbM109PT1cIm1hbmFnZVwiICYmIDxkaXYgY2xhc3NOYW1lPVwidGFicy13cmFwcGVyIG1hbmFnZS1oZWFkZXItdGV4dFwiPlxyXG4gICAgICAgIHtwYXRoQXJyYXlbNF09PT1cImF1dGhrZXlzXCIgPyBcIkF1dGhLZXlzXCIgOiBcIk1hbmFnZSBUZWFtXCJ9XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG4gIH1cclxuXHJcbiAgcmVuZGVyQmFja09wdGlvbigpe1xyXG4gICAgbGV0IHBhdGhBcnJheSA9IHRoaXMucHJvcHMubG9jYXRpb24ucGF0aG5hbWUuc3BsaXQoJy8nKTtcclxuICAgIHJldHVybihcclxuICAgICAgKHBhdGhBcnJheVszXT09PVwibWFuYWdlXCJ8fCBwYXRoQXJyYXlbM109PT1cImJpbGxpbmdcIikgJiYgPGRpdiBjbGFzc05hbWU9XCJsaW5rLW5ldyBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCIgb25DbGljaz17KCkgPT4gdGhpcy5wcm9wcy5oaXN0b3J5LmdvQmFjaygpIH0+IFxyXG4gICAgICAgIDxpbWcgc3JjPXthcnJvd0xlZnR9IGNsYXNzTmFtZT1cInJvdGF0ZS05MCBtci0xMFwiIC8+XHJcbiAgICAgICAge1wiIEJhY2tcIn1cclxuICAgICAgPC9kaXY+IFxyXG4gICAgKVxyXG4gIH1cclxuXHJcbiAgcmVuZGVyKCkge1xyXG4gICAgY29uc3QgcGF0aG5hbWUgPSB0aGlzLnByb3BzLmxvY2F0aW9uLnBhdGhuYW1lO1xyXG4gICAgdGhpcy5wcm9kdWN0ID0gcXVlcnlTdHJpbmcucGFyc2UodGhpcy5wcm9wcy5sb2NhdGlvbi5zZWFyY2gpLnByb2R1Y3RcclxuICAgIGxldCBpc1Byb2plY3REYXNoYm9hcmQgPSBwYXRobmFtZS5pbmNsdWRlcyhcIi9wcm9kdWN0c1wiKVxyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtbJ2hlYWRlck5hdmJhcicsaXNQcm9qZWN0RGFzaGJvYXJkPyAnaGVhZGVyTmF2YmFyV2hpdGUnIDogJyddLmpvaW4oXCIgXCIpfT5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4IGp1c3RpZnktY29udGVudC1iZXR3ZWVuXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibG9nb0NvbnRhaW5lciBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJicmFuZC1ibG9jayBqdXN0aWZ5LWNvbnRlbnQtbGVmdCBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5zaG93RGFzaGJvYXJkQnV0dG9uJiY8ZGl2IGNsYXNzTmFtZT0nc2hvdy1kYXNoYm9hcmQgdGV4dC13aGl0ZSBkLWZsZXgganVzdGlmeS1jb250ZW50LWNlbnRlciAnIG9uQ2xpY2s9eygpID0+IHsgdGhpcy5uYXZpZ2F0ZVRvUHJvZHVjdERhc2hib2FyZCgpfX0+PGltZyBoZWlnaHQ9eycxNnB4J30gd2lkdGg9eycxNnB4J30gc3JjPXthcnJvd30gYWx0PScnIGNsYXNzPVwibXItMTAgcm90YXRlLTI3MFwiIC8+R28gdG8gUHJvZHVjdCBEYXNoYm9hcmQ8L2Rpdj59XHJcbiAgICAgICAgICAgICAgey8qIHshdGhpcy5wcm9kdWN0ICYmIHRoaXMucHJvcHMuc2hvd0JyZWFkQ3J1bWIgJiYgdGhpcy5yZW5kZXJOZXdCcmVhZGNydW1iKCl9ICovfVxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAhaXNQcm9qZWN0RGFzaGJvYXJkICYmIDxkaXYgY2xhc3NOYW1lPSdzd2l0Y2hQcmQgbS0xcic+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHshdGhpcy5wcm9wcy5zaG93RGFzaGJvYXJkQnV0dG9uJiY8RHJvcGRvd24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duLlRvZ2dsZSB2YXJpYW50PSdzdWNjZXNzJyBpZD0nZHJvcGRvd24tYmFzaWMnIGNsYXNzTmFtZT1cImQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICB7IXRoaXMucHJvZHVjdCAmJiB0aGlzLnJlbmRlck5hdmJhckJyYW5kKCl9IEVCTCA8c3ZnIGNsYXNzTmFtZT1cInRyYW5zaXRpb24gbWwtMVwiIHdpZHRoPVwiMTBcIiBoZWlnaHQ9XCI2XCIgdmlld0JveD1cIjAgMCAxMCA2XCIgZmlsbD1cIm5vbmVcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+PHBhdGggZD1cIk0xIDFMNSA1TDkgMVwiIHN0cm9rZT1cIiNFRUVFRUVcIiBzdHJva2Utd2lkdGg9XCIyXCIgc3Ryb2tlLWxpbmVjYXA9XCJyb3VuZFwiIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCIvPjwvc3ZnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd24uVG9nZ2xlPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd24uTWVudT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxEcm9wZG93bi5JdGVtIGhyZWY9JyMnIGNsYXNzTmFtZT0nZHJvcEhlYWRlcic+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFN3aXRjaCB0b1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bi5JdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duLkl0ZW0gaHJlZj0nIycgb25DbGljaz17KCkgPT4geyB0aGlzLnN3aXRjaFByb2R1Y3QoJ2ZlZWRpbycpfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEZlZWRpb1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bi5JdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duLkl0ZW0gaHJlZj0nIycgb25DbGljaz17KCkgPT4geyB0aGlzLnN3aXRjaFByb2R1Y3QoJ2hpdG1hbicpIH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBIaXRtYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd24uSXRlbT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxEcm9wZG93bi5JdGVtIGhyZWY9Jycgb25DbGljaz17KCkgPT4geyB0aGlzLnN3aXRjaFByb2R1Y3QoJ2NvbnRlbnRiYXNlJykgfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIENvbnRlbnRCYXNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Ryb3Bkb3duLkl0ZW0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd24uSXRlbSBocmVmPScnIG9uQ2xpY2s9eygpID0+IHsgdGhpcy5zd2l0Y2hQcm9kdWN0KCdodHRwZHVtcCcpIH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBIdHRwZHVtcFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bi5JdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duLkl0ZW0gaHJlZj0nJyBvbkNsaWNrPXsoKSA9PiB7IHRoaXMubmF2aWdhdGVUb1Byb2R1Y3REYXNoYm9hcmQoKSB9fSBjbGFzc05hbWU9XCJnby10by1saW5rXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgaGVpZ2h0PXsnMTZweCd9IHdpZHRoPXsnMTZweCd9IHNyYz17YXJyb3d9IGFsdD0nJyBjbGFzcz1cIm1yLTEwIHJvdGF0ZS0yNzBcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBHbyB0byBQcm9kdWN0IGRhc2hib2FyZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bi5JdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd24uTWVudT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bj59XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIHshdGhpcy5wcm9kdWN0ICYmIHRoaXMucHJvcHMuc2hvd0JyZWFkQ3J1bWIgJiYgdGhpcy5yZW5kZXJQcm9qZWN0TmFtZSgpfVxyXG4gICAgICAgICAgICAgIHt0aGlzLnJlbmRlckJhY2tPcHRpb24oKX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4XCI+XHJcbiAgICAgICAgICAgICAgey8qIHt0aGlzLnByb3BzLnNob3dGdW5jdGlvblRhYnMgJiYgPE5hdmJhclRhYnMgey4uLnRoaXMucHJvcHN9PjwvTmF2YmFyVGFicz4gfSAqL31cclxuICAgICAgICAgICAgICB7Lyoge3RoaXMucHJvcHMuc2hvd0Z1bmN0aW9uVGFicyAmJiA8ZGl2Pk5hdmJhciAyPC9kaXY+IH0gKi99XHJcbiAgICAgICAgICAgICAgey8qIHt0aGlzLnByb3BzLmJpbGxpbmdzTmF2VGFicyAmJiA8IEJpbGxpbmdOYXZCYXIgey4uLnRoaXMucHJvcHN9Lz59ICovfVxyXG4gICAgICAgICAgICAgIHsvKiB7dGhpcy5wcm9wcy5iaWxsaW5nc05hdlRhYnMgJiYgPGRpdj5CaWxsaW5nIDI8L2Rpdj59ICovfVxyXG4gICAgICAgICAgICAgIHt0aGlzLnJlbmRlck1hbmFnZU5hdmJhckhlYWRpbmcoKX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4IGp1c3RpZnktY29udGVudC1lbmQgbG9nb0NvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgIHsvKiBnbG9iYWwgc2VhcmNoICovfVxyXG4gICAgICAgICAgICAgIHt0aGlzLnJlbmRlclNlYXJjaERyb3Bkb3duKCl9XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIHsvKiB1c2VyIHByb2ZpbGUgKi99XHJcbiAgICAgICAgICAgICAgPFVzZXJQcm9maWxlIHsuLi50aGlzLnByb3BzfT48L1VzZXJQcm9maWxlPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIHsvKiB7dGhpcy5yZW5kZXJOYXZpZ2F0aW9uVGFicygpfSAqL31cclxuICAgICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICAgICk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBOYXZCYXI7XHJcbiJdfQ==